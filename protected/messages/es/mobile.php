<?php
$lang['signin']       =  'Iniciar Sesión';
$lang['username']     =  'Nombre de Usuario';
$lang['password']     =  'Contraseña';
$lang['remember_me']     =  'Recuérdame';
$lang['forgot_password']     =  'Olvidé mi Contraseña';
$lang['enter_your_email']     =  "Introduce tu correo electrónico y te enviaremos un código para reestablecer tu contraseña";
$lang['already_have_password']     =  '¿Ya tienes un Código?';
$lang['submit']     =  'Enviar';
$lang['email']     =  'Correo electrónico';
$lang['code']     =  'Código';
$lang['back']     =  'Atrás';
$lang['new_password']     =  'Nueva Contraseña';
$lang['on_duty']     =  'Activo';
$lang['off_duty']     =  'Fuera de Servicio';
$lang['profile']     =  'Perfil';
$lang['settings']     =  'Configuración';
$lang['logout']     =  'Cerrar Sesión';
$lang['personal']     =  'Personal';
$lang['vehicle']     =  'Vehículo';
$lang['team']     =  'Equipo';
$lang['contact']     =  'Contacto';
$lang['update_profile']     =  'Actualizar Perfil';
$lang['type']     =  'Tipo';
$lang['description']     =  'Descripción';
$lang['license_plate']     =  'Número de Matrícula';
$lang['update_vehicle']     =  'Actualizar Vehículo';
$lang['current_password']     =  'Contraseña Actual';
$lang['new_password']     =  'Nueva Contraseña';
$lang['confirm_password']     =  'Confirmar Contraseña';
$lang['change_password']     =  'Cambiar Contraseña';
$lang['notifications']     =  'Notificaciones';
$lang['enabled_push']     =  'Activar Notificación de Recordatorio';
$lang['select_language']     =  'Selecciona Idioma';
$lang['loading_settings']     =  'Cargando Configuración';
$lang['login_in']     =  'Inicia sesión';
$lang['add_reason']     =  'Añade una razón...';
$lang['task_description']     =  'Descripción de la Tarea';
$lang['accept']     =  'Aceptar';
$lang['decline']     =  'Rechazar';
$lang['start']     =  'Iniciar';
$lang['cancel']     =  'Cancelar';
$lang['arrived']     =  'Llegó';
$lang['cancel']     =  'Cancelar';
$lang['successful']     =  'Exitoso';
$lang['failed']     =  'Fallido';
$lang['view_signature']     =  'Ver Firma';
$lang['add_signature']     =  'Añadir Firma';
$lang['getting_info']     =  'Obteniendo info...';
$lang['task_history']     =  'Historial de Tareas';
$lang['network_error']     =  '¡Ha ocurrido un error de red, por favor intente nuevamente!';
$lang['done']     =  'Listo';
$lang['reset']     =  'Reiniciar';
$lang['view_order_details']     =  'Ver Detalles de la Orden';
$lang['order_details']     =  'Detalles de la Orden';
$lang['ingredients']     =  'Ingredientes';
$lang['discount']     =  'Descuento';
$lang['less_voucher']     =  'Sin Voucher';
$lang['redeem_points']     =  'Redimir puntos';
$lang['sub_total']     =  'Sub Total';
$lang['delivery_fee']     =  'Tasa de Entrega';
$lang['packaging']     =  'Empacado';
$lang['tax']     =  'Impuestos';
$lang['total']     =  'Total';
$lang['no_connection']     =  'Sin conexión a internet';
$lang['turn_off_location']     =  'Has decidido no activar la ubicación exacta';
$lang['net_connection_lost']     =  'Se perdió la conexión a internet';
$lang['device_id']     =  'ID del Dispositivo';
$lang['map']     =  'Mapa';
$lang['you_are_here']     =  'Estás Aquí';
$lang['destination']     =  'Destinación';
$lang['view_map']     =  'Ver Mapa';
$lang['view_direction']     =  'Ver Dirección';
$lang['clear']     =  'Limpiar';
$lang['order_no']     =  'Orden No';
$lang['change']     =  'Cambiar';
$lang['logout_confirm']     =  '¿Estás seguro de que quieres cerrar sesión?';
$lang['close_app']     =  '¿Estás seguro de que quieres cerrar la App?';
$lang['yes']     =  'Sí';
$lang['no']     =  'No';
$lang['name']     =  'Nombre';
$lang['connected']     =  'Conectado';
$lang['tips']     =  'Propinas';
$lang['at']     =  'en';
$lang['language']     =  'idioma';
$lang['calendar_view']     =  'Vista de Calendario';
$lang['color']     =  'Color';

/*version 1.1*/
$lang['software_version']     =  'Versión del Software';
$lang['are_you_sure']     =  '¿Estás seguro?';
$lang['view_notes']       =  'Ver Notas';
$lang['add_notes']        =  'Añadir Notas';
$lang['add_photo']        =  'Añadir Foto';
$lang['view_photo']       =  'Ver Foto';
$lang['pickup_details']   =  'Detalles de la Recogida';
$lang['drop_details']     =  'Detalles de la Entrega';
$lang['take_picture']     =  'Tomar Foto';
$lang['upload_picture']   =  'Actualizar Foto';
$lang['get_photo_failed'] =  'Error al obtener foto';
$lang['photos'] =  'Fotos';
$lang['receive_by'] =  'Recibido por';
$lang['delete_this_photo'] =  '¿Eliminar estas fotos?';
$lang['pickup_details'] =  'Detalles de la Recogida';
$lang['drop_details'] =  'Detalles de la Entrega';

/*version 1.2*/
$lang['tracking'] =  'Monitoreo';
$lang['driver'] =  'Conductor';

/*1.4*/
$lang['completed_task'] =  'tarea completa';
$lang['pending_task'] =  'tarea pendiente';

/*Version 1.6.9 */
$lang['emailaddress'] = 'Correo electrónico';
$lang['phonenumber'] = 'Número de teléfono';
$lang['confirmpassword'] = 'Confirmar contraseña';
$lang['signup'] = 'Regístrate';
$lang['erroremail'] = '¡El correo electrónico es incorrecto!';
$lang['passwordnotmatch'] = 'El campo de confirmación de contraseña no coincide con el campo de contraseña.';
$lang['passwordempty'] = 'La contraseña o la contraseña de confirmación están vacías.';
$lang['createaccount'] = 'Crear una cuenta';
$lang['exitsignin'] = 'Ya tienes una cuenta';

$lang['createaccount'] = 'Crea una cuenta';
$lang['createaccountdesc'] = 'Estás creando una cuenta de Freemium. Sin compromiso, sin costos ocultos y sin complicaciones, precios transparentes y simples.';
$lang['seedifferentplans'] = 'Ver diferentes planes.';

return $lang;