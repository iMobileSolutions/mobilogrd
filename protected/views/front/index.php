
<div class="banner">  
	<div class="col-md-8 border col">
	   <div class="inner border">
	   
	    <h1><?php echo t("Simple, Powerful & Highly Flexible way to")?> <span><?php echo t("optimize your delivery operations")?>.</span></h1>
	    
	    <p><?php echo t("A delivery management software for real-time delivery dispatching, managing & tracking in a smart, simple interface")?>. 
       <!-- <?php echo FrontFunctions::getCompanyName()?> <?php echo t("lets you take back the control and allows you to focus on growing your business")?>. -->
       </p>
	   
	    <div class="line margin"></div>
	    
	    <h3><b><?php echo t("Only now")?>!</b> <?php echo t("Try")?> <?php echo FrontFunctions::getCompanyName()?> <?php echo t("free")?>:</h3>
	    
	    <form id="frm-trytrial" method="POST" onsubmit="return false;">
	    <div class="row">
	      <div class="col-md-5 border">
	          <?php echo CHtml::textField('email_address','',array(
	            'class'=>"rounded3",
	            'placeholder'=>t("Your email address"),
	            'required'=>true
	          ));?>
	      </div> <!--col-->
	      <div class="col-md-5 border">
	          <button type="submit" class="rounded relative yellow-button large">
	          <?php echo t("SIGN UP FOR FREE")?> 
	          <i class="ion-ios-arrow-thin-right"></i>
	          </button>
	      </div> <!--col-->
	    </div><!-- row-->
	    </form>
	    
	    <div class="line margin"></div>
	    
	    <h3><?php echo t("Available on")?>:</h3>
	    
	    <div class="available-wrap">
	     <!--<img src="<?php echo Yii::app()->getBaseUrl(true)."/assets/images-front/web-version.png";?>">
	     <img src="<?php echo Yii::app()->getBaseUrl(true)."/assets/images-front/phone-icon.png";?>">-->
	     <a href="https://apps.apple.com/do/app/mobilog-rd/id1226265639"><i class="ion-social-apple"></i></a>
	     <a href="2.apk" download="2.apk"><i class="ion-social-android"></i></a>
	    </div>
	    
	    
	   </div> <!--inner-->
	</div> <!--col-->
	<div class="col-md-4 border yellow-col col">
	   <img class="phone" src="<?php echo Yii::app()->getBaseUrl(true)."/assets/images-front/phone.png";?>">
	</div> <!--col-->
</div> <!--banner-->


<div class="sections section-1">

  <div class="container border">
      <div class="row">
         <div class="col-md-3 border">
            
            <div class="yellow-col relative">
            <h2><?php echo t("What")?> <?php echo FrontFunctions::getCompanyName()?> <?php echo t("help you do")?>?</h2>
            <div class="line margin dim"></div>
            </div>
           
         </div> <!--col-->
         <div class="col-md-9 border">
         
            <div class="row top150">
              <div class="col-sm-4 border">
                 <img src="<?php echo Yii::app()->getBaseUrl(true)."/assets/images-front/logistic.png";?>">
                 <h4><?php echo t("Streamline the logistics")?></h4>
                 <div class="line margin"></div>
                 <p><?php echo t("An interactive map based interface lets you streamline your entire process from allocation your dispatch and scheduling to tracking a delivery. It enables you locate your workforce on the map in real time")?>.</p>
              </div> <!--col-->
              <div class="col-sm-4 border">
              
                 <img src="<?php echo Yii::app()->getBaseUrl(true)."/assets/images-front/communicate.png";?>">
                 <h4><?php echo t("Communicate seamlessly")?></h4>
                 <div class="line margin"></div>
                 <p><?php echo FrontFunctions::getCompanyName()?> <?php echo t("comes with an integrated 2-way notification which can be used to serve and update your customers about their deliveries")?>.</p>
                
              </div> <!--col-->
              <div class="col-sm-4 border">
              
                 <img src="<?php echo Yii::app()->getBaseUrl(true)."/assets/images-front/driven-decision.png";?>">
                 <h4><?php echo t("Take data driven decisions")?></h4>
                 <div class="line margin"></div>
                 <p><?php echo t("Analytics and graphical report feature available within the dashboard helps you monitor performance of the workforce. Data can be used for decision making to increase customer satisfaction and loyalty")?></p>
              
              </div> <!--col-->
            </div>
         
         </div> <!--col-->
      </div> <!--row-->
  </div> <!--container-->
</div> <!-- section-1-->

<!-- <div class="sections section-2">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-8 border relative">
         
            <div class="inner border">
            <h2><?php echo t("Watch")?> <?php echo FrontFunctions::getCompanyName()?> <?php echo t("in Action")?>!</h2>
            <div class="line margin dim"></div>
            
            <h1><?php echo t("Discover what");?><br/>
            <?php echo FrontFunctions::getCompanyName()?> <?php echo t("can")?> <br/>
            <?php echo t("do for your")?> <br/>
            <?php echo t("business")?></h1>
            
            <a href="<?php echo Yii::app()->createUrl('front/pricing')?>" class="brown-button large relative top30 rounded">
            <?php echo t("SIGN UP FOR FREE")?>
            <i class="ion-ios-arrow-thin-right"></i>
            </a>
            
            <div class="video-wrapper">		
            <img src="<?php echo Yii::app()->getBaseUrl(true)."/assets/images-front/macbook.png";?>">
            </div> 
            </div> 
         </div> 

         
         <div class="col-md-4 border brown-col relative">
            <img class="layer" src="<?php echo Yii::app()->getBaseUrl(true)."/assets/images-front/layer.png";?>">
         </div> 
      </div> 
   </div>
</div>  -->

<div class="sections section-3">
<div class="container border">
    <div class="row">
        <div class="col-md-4 border">
           <h2><?php echo t("Businesses which can use")?> <?php echo FrontFunctions::getCompanyName()?></h2>
           <div class="line margin white"></div>
        </div> <!--col-->
        <div class="col-md-8 border">
            
          <?php if (is_array($services) && count($services)>=1):?>      
          <div class="row">
            <?php $xx=1;?>
            <?php foreach ($services as $val):?>
              <div class="col-sm-4 border">
                 <h4><?php echo t($val['sevices_name'])?></h4>
	             <div class="line margin"></div>
	             
	             <?php if ( is_array($val['sub']) && count($val['sub'])>=1 ):?>
	             <ul>
	             <?php foreach ($val['sub'] as $val2):?>
	                <li>- <?php echo t($val2['sevices_name'])?></li>
	             <?php endforeach;?>
	             </ul>
	             <?php endif;?>
	             
              </div> <!--col-->
              <?php $xx++;?>
            <?php endforeach;?>
          </div> <!--row-->
          <?php endif;?>
           
        </div> <!--col-->
    </div> <!--row-->
</div> <!--container-->
</div> <!--sections-->

<div class="sections section-4">
<div class="container">
   <div class="row">
      <div class="col-md-4 border">
        <h2><?php echo t("Pricing")?></h2>
        <div class="line margin dim"></div>
      </div> <!--col-->
      <div class="col-md-8 border">
         <p class="top20"><?php echo t("Compare our plans side by side to see all the features each plan comes with.")?>
         <br>
         <b><?php echo t("No commitment, no hidden charges and no setup fees.")?></b></p>
      </div> <!--col-->
   </div> <!--row-->
   
   <div class="rowx pricing top20">
      
   
   <?php if (is_array($pricing) && count($pricing)>=1):?>
   <div class="row pricing top20">
   
      <?php foreach ($pricing as $val):?>
      <?php        
       $price=$val['price']; $promo_price=0;
       if($val['promo_price']>0.0001){
       	  $price=$val['promo_price'];
       	  $promo_price=$val['promo_price'];
       }
      ?>
      <div class="col-md-4 border">
         <div class="box">
           <h5><?php echo $val['plan_name']?></h5>
           
           <div class="section">
           <!--<price>0.00 <span>$</span></price>-->
           <?php if($final_price=FrontFunctions::formatPricing($price)):?>
           <?php echo $final_price?>
              <?php if ($promo_price>0):?>
	           <p><?php echo t("Before")?> <span class="promo-price"><?php echo prettyPrice($val['price'])?></span></p>
	           <?php endif;?>
           <?php else :?>
           <price>-</price>
           <?php endif;?>
           <p class="uppercase"><?php echo t("/")?> <?php echo ($val['expiration'] != '1' ? $val['expiration']:'' ); ?> <?php echo t($val['plan_type'])?> </p>
           
           <?php if(!empty($val['plan_name_description'])):?>
           <p class="plan_description readmore"><?php echo $val['plan_name_description']?></p>
           <?php endif;?>
           
           </div>
           
           <div class="section text-left">
             <ul> 
              <li>- <?php echo t("Allowed")." ".t($val['allowed_driver'])." ".t("driver")?>.</li>
              <li>- <?php echo t("Allowed")." ".t($val['allowed_task'])." ".t("Task")?>.</li>
              <?php if ( $val['with_sms']==1):?>
              <li>- <?php echo t("With SMS Features")?></li>
              <?php else :?>
              <li>- <?php echo t("NO SMS Features")?></li>
              <?php endif;?>
              
              <?php if ($val['with_broadcast']==1):?>
              <li>- <?php echo t("With Push Broadcast")?></li>
              <?php endif;?>
              
             </ul>
           </div>
           
           <div class="action">
           <a href="<?php echo Yii::app()->createUrl('front/signup',array( 
             'plan_id'=>$val['plan_id']
           ))?>" class="brown-button large relative top30 rounded">
		   <?php echo t("START NOW")?>
		   <i class="ion-ios-arrow-thin-right"></i>
		   </a>
		   </div>
           
         </div> <!--box-->
      </div> <!--col-->
      <?php endforeach;?>
           
   </div> <!--row-->   
   <?php endif;?>      
   
      
      </div> <!--col-->
   </div> <!--row-->
      
   <img src="<?php echo Yii::app()->getBaseUrl(true)."/assets/images-front/headphone.png";?>">
   
</div> <!--container-->
</div> <!--section-4-->

<!-- <div class="sections section-5">   
</div>  section-5 -->

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d675217eb1a6b0be609e724/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->