<div class="modal fade new-categorie" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button aria-label="Close" data-dismiss="modal" class="close" type="button">
          <span aria-hidden="true"><i class="ion-android-close"></i></span>
        </button>
        <h4 id="mySmallModalLabel" class="modal-title">
          <?php echo t("Add Categorie") ?>
        </h4>
      </div>

      <div class="modal-body">

        <form id="frm" class="frm" method="POST" onsubmit="return false;">
          <?php echo CHtml::hiddenField('action', 'addCategorie') ?>
          <?php echo CHtml::hiddenField('id', '') ?>
          <?php echo CHtml::hiddenField('categorie_photo', '') ?>
          <div class="inner">

            <!-- <div class="row">
              <div class="col-md-6"> -->
                <div class="row top10">
                  <div class="col-xs-9">
                    <?php echo CHtml::textField('name', '', array(
                      'placeholder' => t("Name"),
                      'required' => true
                    )) ?>
                  </div>
                  <div class="col-md-3">


                    <div class="categorie-photo" id="upload-categorie-photo">
                      <p><?php echo t("Categorie Photo") ?></p>
                    </div>

                  </div>
                </div>
                <!--row-->


                <div class="row top10">
                  <div class="col-xs-9">
                    <p><?php echo t("Status") ?></p>
                    <?php
                    echo CHtml::dropDownList('status', '', Driver::driverStatus(), array(
                      'required' => true
                    ));
                    ?>
                  </div>
                </div>

              <!-- </div>
              col -->

              <div class="col-md-6">
                <div class="map-categorie-wrap">

                  <div class="map_task_loader2">
                    <div class="inner">
                      <div class="ploader"></div>
                    </div>
                  </div>
                  <!--map_task_loader-->

                  <div class="map_categorie" id="map_categorie"></div>
                </div>
                <!--map-contact-wrap-->
              </div>
              <!--col-->


            <!-- </div>
            row -->


            <div class="row top20">
              <div class="col-md-5 col-md-offset-7">
                <button type="submit" class="orange-button medium rounded categorie-submit"><?php echo t("Submit") ?></button>
                <button type="button" data-id=".new-categorie" class="close-modal green-button medium rounded"><?php echo t("Cancel") ?></button>
              </div>
            </div>


          </div>
          <!--inner-->
        </form>

      </div>
      <!--body-->

    </div>
  </div>
</div>