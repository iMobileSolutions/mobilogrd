<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<!-- Add to Home Screen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-title" content="Mobilog">
<meta name="theme-color" content="black">
<meta name="mobile-web-app-capable" content="yes">
<!-- Disable translation prompt -->
<meta name="google" value="notranslate">
<meta name="google-play-app" content="app-id=com.imobilesolutions.rmoscat.driver.app">
<meta name="apple-itunes-app" content="app-id=1226265639, affiliate-data=myAffiliateData, app-argument=https://itunes.apple.com/us/app/kartero-rd/id1226265639?mt=8">
<meta name="twitter:card" content="app">
<meta name="twitter:site" content="@Mobilog">
<meta name="twitter:description" content="Mobilog Delivery Management está diseñado para pequeñas y medianas empresas que administran su propio servicio de entrega para una base de clientes establecida y demuestra ser un puente entre su cliente y su personal de entrega.">
<meta name="description" content="Mobilog Delivery Management está diseñado para pequeñas y medianas empresas que administran su propio servicio de entrega para una base de clientes establecida y demuestra ser un puente entre su cliente y su personal de entrega.">
<meta name="twitter:app:country" content="es">
<meta name="twitter:app:name:iphone" content="Mobilog">
<meta name="twitter:app:id:iphone" content="1226265639">
<meta name="twitter:app:url:iphone" content="https://itunes.apple.com/us/app/kartero-rd/id1226265639?mt=8">
<meta name="twitter:app:name:ipad" content="Mobilog">
<meta name="twitter:app:id:ipad" content="1226265639">
<meta name="twitter:app:url:ipad" content="https://itunes.apple.com/us/app/kartero-rd/id1226265639?mt=8">
<meta name="twitter:app:name:googleplay" content="Mobilog">
<meta name="twitter:app:id:googleplay" content="com.imobilesolutions.rmoscat.driver.app">
<meta name="twitter:app:url:googleplay" content="https://play.google.com/store/apps/details?id=com.imobilesolutions.rmoscat.driver.app">
<meta property="al:ios:app_name" content="Mobilog">
<meta property="al:ios:app_store_id" content="1226265639">
<meta property="al:ios:url" content="https://itunes.apple.com/us/app/kartero-rd/id1226265639?mt=8">

<meta property="fb:app_id" content="100016291890210">
<meta property="og:url" content="https://www.mobilogrd.com">
<meta property="og:type" content="website">
<meta property="og:title" content="Mobilog">
<meta property="og:image" content="https://www.mobilogrd.com/touch-icon-iphone-retina.png">
<meta property="og:description" content="Mobilog Delivery Management está diseñado para pequeñas y medianas empresas que administran su propio servicio de entrega para una base de clientes establecida y demuestra ser un puente entre su cliente y su personal de entrega.">
<meta property="og:site_name" content="Mobilog">
<meta property="og:locale" content="es">
<meta property="article:author" content="info@mobilogrd.com">
<meta itemprop="ratingValue" content="4.5">
<meta itemprop="priceCurrency" content="USD">


<!-- iOS app deep linking -->
<!--<meta name="apple-itunes-app" content="app-id=1226265639, app-argument=http/url-sample.com">-->

<!-- Add to Home Screen -->
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<span itemprop="reviewCount" content="924"/>
<link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/com.imobilesolutions.rmoscat.driver.app"/>
<link rel="alternate" href="android-app://com.imobilesolutions.rmoscat.driver.app/http/www.mobilogrd.com"/>
<link rel="shortcut icon" href="<?php echo  Yii::app()->request->baseUrl; ?>/favicon.ico?ver=1.2" />
<link rel="apple-touch-icon" href="/touch-icon-iphone.png"/>
<link rel="apple-touch-icon" sizes="152x152" href="/touch-icon-ipad.png"/>
<link rel="apple-touch-icon" sizes="180x180" href="/touch-icon-iphone-retina.png"/>
<link rel="apple-touch-icon" sizes="167x167" href="/touch-icon-ipad-retina.png"/>
<link rel="apple-touch-startup-image" sizes="2048×1536" href="/screen-ipad-landscape-2x.png" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)"/>
<link rel="apple-touch-startup-image" sizes="1024×768" href="/screen-ipad-landscape.png" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)"/>
<link rel="apple-touch-startup-image" sizes="1536×2048" href="/screen-ipad-portrait-2x.png" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)"/>
<link rel="apple-touch-startup-image" sizes="768×1024" href="/screen-ipad-portrait.png" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)"/>
<link rel="apple-touch-startup-image" sizes="640x1136" href="/screen-iphone-568h-2x.png" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)"/>
<link rel="apple-touch-startup-image" sizes="2208×1242" href="/screen-iphone-landscape-736h.png" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)"/>
<link rel="apple-touch-startup-image" sizes="640×960" href="/screen-iphone-portrait-2x.png" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)"/>
<link rel="apple-touch-startup-image" sizes="750×1334" href="/screen-iphone-portrait-667h.png" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)"/>
<link rel="apple-touch-startup-image" sizes="1242×2208" href="/screen-iphone-portrait-736h.png" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)"/>
<link rel="apple-touch-startup-image" sizes="320x480" href="/screen-iphone-portrait.png" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)"/>
</head>
