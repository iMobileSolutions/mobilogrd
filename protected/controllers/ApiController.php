<?php
class ApiController extends CController
{	
	public $data;
	public $code=2;
	public $msg='';
	public $details='';
  
	public function __construct()
	{
		$this->data=$_GET;
		
		// $app_version = isset($_REQUEST['app_version'])?$_REQUEST['app_version']:'';
		// if($app_version>="1.5.0"){
		// 	$this->data=$_POST;
		// }
		
		$website_timezone=Yii::app()->functions->getOptionAdmin("website_timezone");		 
	    if (!empty($website_timezone)){
	 	   Yii::app()->timeZone=$website_timezone;
	    }		 
	    
	    if(isset($_REQUEST['lang_id'])){
		 	Yii::app()->language=$_REQUEST['lang_id'];
		}	    
	}
	
	public function beforeAction($action)
	{				
		/*check if there is api has key*/	
		$action=Yii::app()->controller->action->id;
		$continue=true;		
		$action=strtolower($action);
		if($action=="getlanguagesettings" || $action=="getappsettings" || $action=="uploadprofile" || $action=="uploadtaskphoto" || $action=="locations" || $action=="updatedriverlocation" || $action == "UpdateOnlineM") {
	   	   $continue=false;
	    }	
	    if($continue){	    	
	    	$key=getOptionA('mobile_api_key');
	    	if(!empty($key)){
		    	if(!isset($this->data['api_key'])){
		    		$this->data['api_key']='';
		    	}
		    	if(trim($key)!=trim($this->data['api_key'])){
				   $this->msg=$this->t("api hash key is not valid");
			       $this->output();
			       Yii::app()->end();
				}
	    	}
	    }
		return true;
	}	
	
	public function actionIndex(){
		echo 'Api is working';
	}		
	
	private function q($data='')
	{
		return Yii::app()->db->quoteValue($data);
	}
	
	private function t($message='')
	{
		return Yii::t("default",$message);
	}
		
    private function output()
    {
    	
       if (!isset($this->data['debug'])){    		
       	  header('Access-Control-Allow-Origin: *');
          header('Content-type: application/javascript;charset=utf-8');
       } 
       
	   $resp=array(
	     'code'=>$this->code,
	     'msg'=>$this->msg,
	     'details'=>$this->details,
	     'request'=>json_encode($this->data,JSON_UNESCAPED_UNICODE)	  
	   );		   
	   if (isset($this->data['debug'])){
	   	   dump($resp);
	   }
	   
	   if (!isset($_GET['callback'])){
  	   	   $_GET['callback']='';
	   }    

	      
	//    $app_version = isset($_REQUEST['app_version'])?$_REQUEST['app_version']:'';	   
	//    if($app_version>="2.0.0"){
	//    	  echo CJSON::encode($resp);
	//    	  Yii::app()->end();
	//    }
	   
	   if (isset($_GET['json']) && $_GET['json']==TRUE){
	   	   echo CJSON::encode($resp);
	   } else echo $_GET['callback'] . '('.CJSON::encode($resp).')';		    	   	   	  
	   Yii::app()->end();
    }			
    
    public function actionLoginM()
    {
		
		if (empty($this->data['device_id'])){
    		$this->msg="";
    		$this->output();
    		Yii::app()->end();
		}
		
    	if(!empty($this->data['username']) && !empty($this->data['password'])){
	    	if ( $res=Driver::driverAppLoginNotStatus($this->data['username'],$this->data['password']))
				{
					// error_log($res);

					if ($vStatus=Driver::driverAppLoginLockedAccount(3,$res['customer_id'],$res['team_id'],$res['driver_id'])) {
						if ($vStatus['device_id']!=$this->data['device_id']){
							$this->code=9;
							$this->msg="Your user is not authorized to be available on multiple devices";
							$this->output();
							Yii::app()->end();
						}
					} else if ($vStatus=Driver::driverAppLoginLockedAccount(1,$this->data['device_id'])) {

						if (($vStatus['customer_id']!=$res['customer_id'])
							|| ($vStatus['team_id']!=$res['team_id'] 
							|| ($vStatus['driver_id']!=$res['driver_id']))  ){
							$this->code=10;
							$this->msg="Your user is not authorized to be available on multiple devices";
							$this->output();
							Yii::app()->end();
						}
					}

					if ($vares=Driver::verificationAccount($res['customer_id'])){
							if ($vares['status']=='active') {
									if ($res['status']=='active')
									{
										
										if(empty($res['hashtoken'])){
												$token=md5(Driver::generateRandomNumber(5) . $this->data['username']);
												$admin=0;
                        error_log("NOT ADMNISTRADOR!", 0);
										} else {
												$admin=1;
                        error_log("ADMINISTRADOR!", 0);
												$token=$res['hashtoken'];
										}
	    		$customer_id=$res['customer_id'];	    
	    		Driver::setCustomerTimezone($customer_id);
	    	
										  $icharging=0;
											if ($this->data['battery_status'] == true) $icharging=1;

										  $batterylevel= -1;
										 if (!empty($this->data['battery_level'])) {
												$batterylevel = $this->data['battery_level'] * 100;
											}


										
	    		$params=array(
	    		  'last_login'=>AdminFunctions::dateNow(),
	    		  'last_online'=>strtotime("now"),
	    		  'ip_address'=>$_SERVER['REMOTE_ADDR'],
	    		  'token'=>$token,
	    		  'device_id'=>isset($this->data['device_id'])?$this->data['device_id']:'',
	    		  'device_platform'=>isset($this->data['device_platform'])?$this->data['device_platform']:'Android',
	    		  'on_duty'=>1,
	    		  'is_online'=>1,
	    		  'app_version'=>isset($this->data['app_version'])?$this->data['app_version']:'',
			 	 'enabled_push'=>1,
			      'battery_level'=>$batterylevel,
                          'battery_status'=>$icharging,
                          'activity_confidence'=>isset($this->data['activity_confidence'])?$this->data['activity_confidence']:'-1',
                          'activity_type'=>isset($this->data['activity_type'])?$this->data['activity_type']:''
	    		);	    		
	    		
	    		$res['on_duty']=1;
	    		
	    		if(!empty($res['token'])){
	    			unset($params['token']);
	    			$token=$res['token'];
	    		}
	    		$db=new DbExt;
	    		if ( $db->updateData("{{driver}}",$params,'driver_id',$res['driver_id'])){	    			
	    			$this->code=1;
	    			$this->msg=self::t("Login Successful");
	    			
	    			//get location accuracy
	    			$location_accuracy=2;
	    			if ( $team=Driver::getTeam($res['team_id'])){
	    				//dump($team);
	    				if($team['location_accuracy']=="high"){
	    					$location_accuracy=1;
	    				}
	    			}
											$app_track_interval=getOption($res['customer_id'],'app_track_interval');
											if (!is_numeric($app_track_interval)){
												$app_track_interval=8000;
											} else $app_track_interval=$app_track_interval*1000;

											if ($app_track_interval<=0){
												$app_track_interval=8000;
											}

											$this->details=array(
												'username'=>$this->data['username'],
												'password'=>$this->data['password'],
												'remember'=>isset($this->data['remember'])?$this->data['remember']:'on',
												'todays_date'=>Yii::app()->functions->translateDate(date("M, d")),
												'todays_date_raw'=>date("Y-m-d"),
												'on_duty'=>$res['on_duty'],
												'token'=>$token,
												'duty_status'=>$res['on_duty'],
												'location_accuracy'=>$location_accuracy,
												'device_vibration'=>getOption($res['customer_id'],'driver_device_vibration'),
												'app_disabled_bg_tracking'=>getOption($res['customer_id'],'app_disabled_bg_tracking'),
												'app_track_interval'=>$app_track_interval,
												'admin'=>$admin,
												'enabled_push'=>(integer)$res['enabled_push'],
	    										 'topic_new_task'=>str_replace("/topics/","",CHANNEL_TOPIC).$topic_id,
	    									     'topic_alert'=>str_replace("/topics/","",CHANNEL_TOPIC_ALERT).$topic_id
											);
										} else $this->msg=self::t("Login failed. please try again later");
									} else $this->msg=self::t("Login failed.") . Driver::driverStatusLoginPretty($res['status']);
							} else  {
									if ($vares['status']=='waitconfim') {
												$this->code=3;  // Codigo Error para aplicacion una Accion Verificar account;
												$this->details=array(
													'username'=>$res['username'],
													'password'=>$res['password'],
													'token'=>$res['token']
												);
												$this->msg=self::t("Please contact the administrator the account is not verified");
									} else {
										//$this->code=2;  // Codigo Error para aplicacion una Accion Verificar account;
										$this->msg=self::t("Please contact the administrator the account. Status") . $vares['status'];
									}
							}  
					} else $this->msg=self::t("Verification account error.");
				} else $this->msg=self::t("Login failed. either username or password is incorrect");
    	} else $this->msg=self::t("Please fill in your username and password");
    	$this->output();
	}
	
	public function actionLogin()
    {
    	if(!empty($this->data['username']) && !empty($this->data['password'])){
	    	if ( $res=Driver::driverAppLogin($this->data['username'],$this->data['password'])){	
	    		$token=md5(Driver::generateRandomNumber(5) . $this->data['username']);
	    		
	    		$customer_id=$res['customer_id'];	    
	    		Driver::setCustomerTimezone($customer_id);
	    		
	    		$params=array(
	    		  'last_login'=>AdminFunctions::dateNow(),
	    		  'last_online'=>strtotime("now"),
	    		  'ip_address'=>$_SERVER['REMOTE_ADDR'],
	    		  'token'=>$token,
	    		  'device_id'=>isset($this->data['device_id'])?$this->data['device_id']:'',
	    		  'device_platform'=>isset($this->data['device_platform'])?$this->data['device_platform']:'Android',
	    		  'on_duty'=>1,
	    		  'is_online'=>1,
	    		  'app_version'=>isset($this->data['app_version'])?$this->data['app_version']:'',
	    		  'enabled_push'=>1
	    		);	    		
	    		
	    		$res['on_duty']=1;
	    		
	    		if(!empty($res['token'])){
	    			unset($params['token']);
	    			$token=$res['token'];
	    		}
	    		
	    		$up = Yii::app()->db->createCommand()->update("{{driver}}",$params,
		  	    'driver_id=:driver_id',
			  	    array(
			  	      ':driver_id'=>$res['driver_id']
			  	    )
		  	    );	    		
	    		if($up){
	    			$this->code=1;
	    			$this->msg=self::t("Login Successful");
	    			
	    			//get location accuracy
	    			$location_accuracy=2;
	    			if ( $team=Driver::getTeam($res['team_id'])){	    				
	    				if($team['location_accuracy']=="high"){
	    					$location_accuracy=1;
	    				}
	    			}
	    			
	    			$app_track_interval=getOption($res['customer_id'],'app_track_interval');
	    			if (!is_numeric($app_track_interval)){
	    				$app_track_interval=60000;
	    			} else $app_track_interval=$app_track_interval*1000;
	    			
	    			if ($app_track_interval<=0){
	    				$app_track_interval=60000;
	    			}
	    			
	    			$topic_id = (integer) $res['customer_id'];
	    			
	    			$this->details=array(
	    			  'username'=>$this->data['username'],
	    			  'password'=>$this->data['password'],
	    			  'remember'=>isset($this->data['remember'])?$this->data['remember']:'',
	    			  'todays_date'=>Yii::app()->functions->translateDate(date("M, d")),
	    			  'todays_date_raw'=>date("Y-m-d"),
	    			  'on_duty'=>$res['on_duty'],
	    			  'token'=>$token,
	    			  'duty_status'=>$res['on_duty'],
	    			  'location_accuracy'=>$location_accuracy,
	    			  'device_vibration'=>getOption($res['customer_id'],'driver_device_vibration'),
	    			  'app_disabled_bg_tracking'=>getOption($res['customer_id'],'app_disabled_bg_tracking'),
	    			  'app_track_interval'=>$app_track_interval,
	    			  'enabled_push'=>(integer)$res['enabled_push'],
	    			  'topic_new_task'=>str_replace("/topics/","",CHANNEL_TOPIC).$topic_id,
	    			  'topic_alert'=>str_replace("/topics/","",CHANNEL_TOPIC_ALERT).$topic_id,
	    			);	    		
	    				    			
	    		} else $this->msg=self::t("Login failed. please try again later");
	    	} else $this->msg=self::t("Login failed. either username or password is incorrect");
    	} else $this->msg=self::t("Please fill in your username and password");
    	$this->output();
    }
	
  public function actionLoginVerification()
    {
			if(!empty($this->data['username']) && !empty($this->data['password'])){
	    	if ( $res=Driver::driverAppLoginNotStatus($this->data['username'],$this->data['password']))
				{
					if ($vares=Driver::verificationAccount($res['customer_id'])){
							if ($vares['status']=='active') {
									if ($res['status']=='active')
									{
										
										if(empty($res['hashtoken'])){
												$admin=0;
										} else {
												$admin=1;
										}
										
										$customer_id=$res['customer_id'];	    
										Driver::setCustomerTimezone($customer_id);

										$params=array(
                      'last_login'=>AdminFunctions::dateNow(),
											'last_online'=>strtotime("now"),
											'ip_address'=>$_SERVER['REMOTE_ADDR'],
                      'battery_level'=>isset($this->data['battery_level'])?$this->data['battery_level']:'-1',
							        'battery_status'=>isset($this->data['battery_status'])?$this->data['battery_status']:'false',	
                      'activity_confidence'=>$this->data['activity_confidence'],
                      'activity_type'=>$this->data['activity_type'],
										);	    		
                  //	$res['on_duty']=1;
                
										if(!empty($res['token'])){
											unset($params['token']);
											$token=$res['token'];
										}
                    //error_log("PASO1!---> ".strtotime("now"), 0);
                    
										$db=new DbExt;
										if ( $db->updateData("{{driver}}",$params,'driver_id',$res['driver_id'])){
                      //error_log("PASO2!---> ".strtotime("now"), 0);
											$this->code=1;
											$this->msg=self::t("Login Verification Successful");

											//get location accuracy
											$location_accuracy=2;
											if ( $team=Driver::getTeam($res['team_id'])){
												//dump($team);
												if($team['location_accuracy']=="high"){
													$location_accuracy=1;
												}
											}

											$app_track_interval=getOption($res['customer_id'],'app_track_interval');
											if (!is_numeric($app_track_interval)){
												$app_track_interval=8000;
											} else $app_track_interval=$app_track_interval*1000;

											if ($app_track_interval<=0){
												$app_track_interval=8000;
											}
                      //error_log("PASO3!---> ".strtotime("now"), 0);
											$this->details=array(
												'username'=>$this->data['username'],
												'password'=>$this->data['password'],
												'remember'=>isset($this->data['remember'])?$this->data['remember']:'on',
												'todays_date'=>Yii::app()->functions->translateDate(date("M, d")),
												'todays_date_raw'=>date("Y-m-d"),
												'on_duty'=>$res['on_duty'],
												'token'=>$token,
												'duty_status'=>$res['on_duty'],
												'location_accuracy'=>$location_accuracy,
												'device_vibration'=>getOption($res['customer_id'],'driver_device_vibration'),
												'app_disabled_bg_tracking'=>getOption($res['customer_id'],'app_disabled_bg_tracking'),
												'app_track_interval'=>$app_track_interval,
												'admin'=>$admin
											);
										}
									} else { $this->code=4; $this->msg=self::t("Login verification failed.") . Driver::driverStatusLoginPretty($res['status']); }
							} else  {
									if ($vares['status']=='waitconfim') {
												$this->code=3;  // Codigo Error para aplicacion una Accion Verificar account;
												$this->details=array(
													'username'=>$res['username'],
													'password'=>$res['password'],
													'token'=>$res['token']
												);
												$this->msg=self::t("Please contact the administrator the account is not verified");
									} else {
										//$this->code=2;  // Codigo Error para aplicacion una Accion Verificar account;
										$this->msg=self::t("Please contact the administrator the account. Status") . $vares['status'];
									}
							}  
					} else $this->msg=self::t("Verification account error.");
				} else { $this->code=4;  $this->msg=self::t("Login verification failed. either username or password is incorrect"); }
    	} else { $this->code=5; $this->msg=self::t("Please fill in your username and password"); }
    	$this->output();
    }
  
	 public function actionLoginVerificationM()
    {
		$js_code = 'console.log actionLoginVerificationM (' . json_encode($this->data, JSON_HEX_TAG) . ');';
		error_log($js_code);
		
			if(!empty($this->data['token'])){
	    	if ( $res=Driver::driverAppLoginNotStatusToken($this->data['token']))
				{
					if ($vares=Driver::verificationAccount($res['customer_id'])){
							if ($vares['status']=='active') {
									if ($res['status']=='active')
									{

										if ($this->data['event_type'] == "autologin") return;
										
										if(empty($res['hashtoken'])){
												$admin=0;
										} else {
												$admin=1;
										}
										
										$customer_id=$res['customer_id'];	    
										Driver::setCustomerTimezone($customer_id);

										 $icharging=0;
											if ($this->data['battery_status'] == true) $icharging=1;

										 if (!empty($this->data['battery_level'])) {
												$batterylevel = $this->data['battery_level'] * 100;
											}
										
											$ismoving=0;
											if ($this->data['is_moving'] == true) $ismoving=1;
										
										$params=array(
                     						 'last_login'=>AdminFunctions::dateNow(),
											'last_online'=>strtotime("now"),
											'ip_address'=>$_SERVER['REMOTE_ADDR'],
                      'battery_level'=>$batterylevel,
							        'battery_status'=>$icharging,	
                      'activity_confidence'=>isset($this->data['activity_confidence'])?$this->data['activity_confidence']:'-1',
                      'activity_type'=>isset($this->data['activity_type'])?$this->data['activity_type']:'0',
											'event_type'=>$this->data['event_type'],
											'device_id'=>$this->data['device_id']
										);	    		
                
										if(!empty($res['token'])){
											unset($params['token']);
											$token=$res['token'];
										}
                    
										$db=new DbExt;
										if ( $db->updateData("{{driver}}",$params,'driver_id',$res['driver_id'])){
               							$this->code=1;
											$this->msg=self::t("Login Verification Successful");

											//get location accuracy
											$location_accuracy=2;
											if ( $team=Driver::getTeam($res['team_id'])){
												//dump($team);
												if($team['location_accuracy']=="high"){
													$location_accuracy=1;
												}
											}

											$app_track_interval=getOption($res['customer_id'],'app_track_interval');
											if (!is_numeric($app_track_interval)){
												$app_track_interval=8000;
											} else $app_track_interval=$app_track_interval*1000;

											if ($app_track_interval<=0){
												$app_track_interval=8000;
											}
											
			$is_record=getOption($customer_id,'agents_record_track_Location');
    		if ($is_record==1){
          
//           $vtimestamp=substr(str_replace('T','-',$obj['timestamp']));
	    		$logs=array(
	    		  'customer_id'=>$customer_id,
	    		  'driver_id'=>$this->data['device_id'],
	    		  'latitude'=>$this->data['latitude'],
	    	    'longitude'=>$this->data['longitude'],
	    	      'altitude'=>isset($this->data['altitude'])?$this->data['altitude']:'',
	    	      'accuracy'=>isset($this->data['accuracy'])?$this->data['accuracy']:'',
	    	      'altitudeAccuracy'=>isset($this->data['altitude_accuracy'])?$this->data['altitude_accuracy']:'',
	    	      'heading'=>isset($this->data['heading'])?$this->data['heading']:'',
	    	      'speed'=>isset($this->data['speed'])?$this->data['speed']:'',
	    	      'track_type'=>isset($this->data['track_type'])?$this->data['track_type']:'', 
              'battery_level'=>$batterylevel,//isset($obj['battery']['level'])?$obj['battery']['level']:'-1',
              'battery_status'=>$icharging,
              'activity_confidence'=>isset($this->data['confidence'])?$this->data['confidence']:'-1',
              'activity_type'=>isset($this->data['type'])?$this->data['type']:'',
              'geofence_identifier'=>isset($this->data['identifier'])?$this->data['identifier']:'',
              'geofence_action'=>isset($this->data['action'])?$this->data['action']:'',
              'uuid'=>isset($this->data['uuid'])?$this->data['uuid']:'',
              'event'=>isset($this->data['event_type'])?$this->data['event_type']:'',
              'is_moving'=>$ismoving,
              'odometer'=>isset($this->data['odometer'])?$this->data['odometer']:'-1',
              'timestamp'=>isset($this->data['timestamp'])?$this->data['timestamp']:AdminFunctions::dateNow(),
              'date_created'=>AdminFunctions::dateNow(),
	    	      'ip_address'=>$_SERVER['REMOTE_ADDR'],	
	    	      'date_log'=>date("Y-m-d")
	    		);
// 					  $js_code = 'console.log(' . json_encode($logs, JSON_HEX_TAG) . ');';
//         	error_log($js_code);
					
				// $db->insertData("{{driver_track_location}}",$logs);
				Yii::app()->db->createCommand()->insert("{{driver_track_location}}",$logs);

					  $js_code = 'console.4(' . json_encode($logs, JSON_HEX_TAG) . ');';
				  error_log($js_code);
			}    		
			
                      //error_log("PASO3!---> ".strtotime("now"), 0);
											$this->details=array(
// 												'username'=>$this->data['username'],
// 												'password'=>$this->data['password'],
												'remember'=>isset($this->data['remember'])?$this->data['remember']:'on',
												'todays_date'=>Yii::app()->functions->translateDate(date("M, d")),
												'todays_date_raw'=>date("Y-m-d"),
												'on_duty'=>$res['on_duty'],
												'token'=>$token,
												'duty_status'=>$res['on_duty'],
												'location_accuracy'=>$location_accuracy,
												'device_vibration'=>getOption($res['customer_id'],'driver_device_vibration'),
												'app_disabled_bg_tracking'=>getOption($res['customer_id'],'app_disabled_bg_tracking'),
												'app_track_interval'=>$app_track_interval,
												'admin'=>$admin
											);
											
										}
									} else { $this->code=4; $this->msg=self::t("Login verification failed.") . Driver::driverStatusLoginPretty($res['status']); }
							} else  {
									if ($vares['status']=='waitconfim') {
										if ($vares['token']==$res['token']){
											$this->code=3;  // Codigo Error para aplicacion una Accion Verificar account;
											$this->details=array(
												'username'=>$res['username'],
												'password'=>$res['password'],
												'token'=>$res['token']
											);
											$this->msg=self::t("Please the account is not verified");
											ResendCode($vares['token']);
									  } else {
											$this->code=3;  // Codigo Error para aplicacion una Accion Verificar account;
											$this->details=array(
												'username'=>$res['username'],
												'password'=>$res['password'],
												'token'=>$res['token']
											);
											$this->msg=self::t("Please contact the administrator the account is not verified");
										}
									} else {
										//$this->code=2;  // Codigo Error para aplicacion una Accion Verificar account;
										$this->msg=self::t("Please contact the administrator the account. Status") . $vares['status'];
									}
							}  
					} else{  $this->code=7; $this->msg=self::t("Verification account error or not exit"); }
				} else { $this->code=5;  $this->msg=self::t("Token not valid"); }
    	} else { $this->code=6; $this->msg=self::t("Token empty"); }
    	$this->output();
    }
	
	
	public function actionresendVerification()
	{
		
		if ($res=FrontFunctions::getCustomerByToken($this->data['token'])){
			
			$verification_code=$res['verification_code'];
			$signup_verification=getOptionA('signup_verification');
			if($signup_verification=="mail"){				
				FrontFunctions::sendEmailSignVerification($res,$verification_code);
				$this->code=1;
				$this->msg=t("We have sent your verification code to your email");
			} else if ($signup_verification=="sms") {
				
				$signup_tpl_sms=getOptionA('signup_tpl_sms');
				if(!empty($signup_tpl_sms) && !empty($res['mobile_number']) ){
					$company_name=getOptionA('company_name');
					$signup_tpl_sms=smarty('first_name',$res['first_name'],$signup_tpl_sms);
					$signup_tpl_sms=smarty('first_name',$res['first_name'],$signup_tpl_sms);
					$signup_tpl_sms=smarty('verification_code',$verification_code,$signup_tpl_sms);
					$signup_tpl_sms=smarty('company_name',$company_name,$signup_tpl_sms);
					sendSMS($res['mobile_number'],$signup_tpl_sms);					
					$this->code=1;
				    $this->msg=t("We have sent your verification code to your mobile");
				} else $this->msg=t("SMS template not available");
				
			} else $this->msg=t("Invalid verification type");
		} else $this->msg=t("hash not found");
		$this->output();
	}
	
	public function actionverifySignupCodeM()
	{		
		if($rescustomer=FrontFunctions::getCustomerByToken($this->data['token'])){			
			$customer_id=$rescustomer['customer_id'];
			
			if ( $rescustomer['verification_code']==$this->data['verification_code']){
				
				if ( $res=Driver::driverAppLoginNotStatus($this->data['username'],$this->data['password']))
				{
		
					if ($res['status']=='active')
					{
						$token=md5(Driver::generateRandomNumber(5) . $this->data['username']);

						$customer_id=$res['customer_id'];	    
						Driver::setCustomerTimezone($customer_id);

						$params=array(
							'last_login'=>AdminFunctions::dateNow(),
							'last_online'=>strtotime("now"),
							'ip_address'=>$_SERVER['REMOTE_ADDR'],
							'token'=>$token,
							'device_id'=>isset($this->data['device_id'])?$this->data['device_id']:'',
							'device_platform'=>isset($this->data['device_platform'])?$this->data['device_platform']:'Android',
							'on_duty'=>1
						);	    		

						$res['on_duty']=1;

						if(!empty($res['token'])){
							unset($params['token']);
							$token=$res['token'];
						}
						$db=new DbExt;
						if ( $db->updateData("{{driver}}",$params,'driver_id',$res['driver_id'])){	    
								$this->code=1;
								$this->msg=t("Successful");

								$signup_needs_approval=getOptionA('signup_needs_approval');

								$db=new DbExt();
								$db->updateData("{{customer}}",array(
									'status'=>'active',
									'verification_confirm_date'=>AdminFunctions::dateNow()
								),'customer_id',$customer_id);
							
								//get location accuracy
								$location_accuracy=2;
								if ( $team=Driver::getTeam($res['team_id'])){
									//dump($team);
									if($team['location_accuracy']=="high"){
										$location_accuracy=1;
									}
								}
	    			$app_track_interval=getOption($res['customer_id'],'app_track_interval');
	    			if (!is_numeric($app_track_interval)){
	    				$app_track_interval=60000;
	    			} else $app_track_interval=$app_track_interval*1000;
	    			
	    			if ($app_track_interval<=0){
	    				$app_track_interval=60000;
	    			}
	    			
								if ( $res=Driver::driverAppLoginNotStatus($this->data['username'],$this->data['password'])){
	    			$this->details=array(
	    			  'username'=>$this->data['username'],
	    			  'password'=>$this->data['password'],
	    			  'remember'=>isset($this->data['remember'])?$this->data['remember']:'',
	    			  'todays_date'=>Yii::app()->functions->translateDate(date("M, d")),
	    			  'todays_date_raw'=>date("Y-m-d"),
	    			  'on_duty'=>$res['on_duty'],
	    			  'token'=>$token,
	    			  'duty_status'=>$res['on_duty'],
	    			  'location_accuracy'=>$location_accuracy,
	    			  'device_vibration'=>getOption($res['customer_id'],'driver_device_vibration'),
	    			  'app_disabled_bg_tracking'=>getOption($res['customer_id'],'app_disabled_bg_tracking'),
	    			  'app_track_interval'=>$app_track_interval,
	    			  'enabled_push'=>(integer)$res['enabled_push'],
	    			  'topic_new_task'=>str_replace("/topics/","",CHANNEL_TOPIC).$topic_id,
	    			  'topic_alert'=>str_replace("/topics/","",CHANNEL_TOPIC_ALERT).$topic_id,
	    			);
									}
							
						  FrontFunctions::sendEmailWelcome($rescustomer);
						} else $this->msg=self::t("Login failed. please try again later");
					} else $this->msg=self::t("Login failed.") . Driver::driverStatusLoginPretty($res['status']);
				} else $this->msg=self::t("Login failed. either username or password is incorrect");
			} else $this->msg=t("Verification code is invalid");
		} else $this->msg=t("Token not found");
		$this->output();
	}
	
	 public function actionCreateAccountM()
    {
			$params=$this->data;
			$customer_params['email_address']=$params['email_address'];
			$customer_params['mobile_number']=$params['mobile_number'];
			$customer_params['password']=$params['password'];
			$customer_params['company_name']=$params['company_name'];
			$customer_params['first_name']=$params['first_name'];
			$customer_params['last_name']=$params['last_name'];
			$customer_params['plan_id']=$params['plan_id'];
			$customer_params['status']='waitconfim';
			$customer_params['type_singup']='mobile';
		  $customer_params['country_code']=$params['country_code'];
		
				
			unset($params['action']);
			unset($params['cpassword']);
			unset($params['confirmpassword']);
			$customer_params['date_created']=AdminFunctions::dateNow();
			$customer_params['ip_address']=$_SERVER['REMOTE_ADDR'];				
			
			$encryption_type=Yii::app()->params->encryption_type;
			if(empty($encryption_type)){
				$encryption_type='yii';
			}
			
			if ( $encryption_type=="yii"){
				$customer_params['password']=CPasswordHelper::hashPassword($customer_params['password']);	
			} else $customer_params['password']=md5($customer_params['password']);
			
// 			$customer_params['password']=md5($customer_params['password']);
			
			$plan_price=Driver::getPlansPrice($this->data['plan_id']);
			$token=md5(AdminFunctions::generateCode(10));
			$verification_code=AdminFunctions::generateNumericCode(5);
			$customer_params['token']=$token;
			$customer_params['verification_code']=$verification_code;
			if ($plan_details=FrontFunctions::getPlansByID($customer_params['plan_id'])){							
				$price=$plan_details['price'];
// 				if($plan_details['promo_price']>0.0001){
// 					$price=$plan_details['promo_price'];
// 				}
				$customer_params['plan_price']=$price;
				$days=$plan_details['expiration'];
				$plan_type=$plan_details['plan_type'];
				$customer_params['plan_expiration']=date("Y-m-d",strtotime("+$days $plan_type"));
				$customer_params['plan_currency_code']=Driver::getCurrenyCode(true);
				$customer_params['with_sms']=$plan_details['with_sms'];
				$customer_params['sms_limit']=$plan_details['sms_limit'];
				$customer_params['with_broadcast']=$plan_details['with_broadcast'];
				
				$customer_params['no_allowed_driver']=$plan_details['allowed_driver'];
				$customer_params['no_allowed_task']=$plan_details['allowed_task'];		
			
			}
			
    	$db=new DbExt();
			if ( !Driver::getCustomerByEmail($this->data['email_address'])){
				if ( $db->insertData("{{customer}}",$customer_params)){
					$customer_id=Yii::app()->db->getLastInsertID();
					$customer_params['customer_id']=$customer_id;
					self::setGenetalSettingsDefault($customer_params);
					
					$driver_team_params['customer_id']=$customer_id;
					$driver_team_params['team_name']='Team A';
					$driver_team_params['location_accuracy']='high';
					$driver_team_params['status']='publish';
					$driver_team_params['date_created']=AdminFunctions::dateNow();
					$driver_team_params['ip_address']=$_SERVER['REMOTE_ADDR'];
					
					if($db->insertData("{{driver_team}}",$driver_team_params)){
						$team_id=Yii::app()->db->getLastInsertID();
						$driver_params['customer_id']=$customer_id;
						$driver_params['on_duty']=1;
						$driver_params['first_name']="";
						$driver_params['last_name']="";
						$driver_params['email']=$params['email_address'];
					//	$driver_params['phone']="";//$params['mobile_number'];
						$driver_params['username']=$params['email_address'];
						$driver_params['password']=$customer_params['password'];
						$driver_params['team_id']=$team_id;
						$driver_params['transport_type_id']='car';
						$driver_params['transport_description']='car';
						$driver_params['status']='active';
						$driver_params['date_created']=AdminFunctions::dateNow();
 						$driver_params['location_address']='';
 						$driver_params['location_lat']=$params['location_lat'];
 						$driver_params['location_lng']=$params['location_lng'];
						$driver_params['ip_address']=$_SERVER['REMOTE_ADDR'];
					 	$driver_params['token']=$token;//md5(AdminFunctions::generateCode(10));
 						$driver_params['device_id']=$params['device_id'];
 						$driver_params['device_platform']=$params['device_platform'];
						$driver_params['enabled_push']=1;
						$driver_params['app_version']=$params['app_version'];
						$driver_params['battery_level']=$params['battery_level'];
						$driver_params['battery_status']=$params['battery_status'];
					  $driver_params['last_onduty']='';
						
							if($db->insertData("{{driver}}",$driver_params)){
								$driver_id=Yii::app()->db->getLastInsertID();

		// 						$this->code=1;
		// 						$this->msg=Driver::t("Successful");
		// 						$this->details='create-team';

								$this->code=1;
								$this->msg=t("Registration successul");

								$signup_needs_approval=getOptionA('signup_needs_approval');
								$signup_verification_enabled=getOptionA('signup_verification_enabled');
								$signup_verification=getOptionA('signup_verification');

								if ( $signup_needs_approval==1){
		// 							$this->details=Yii::app()->createUrl('front/signupty',array(
		// 							   'hash'=>$token,
		// 							   'needs_approval'=>1
		// 							));			
									$this->details=array(
														'username'=>$customer_params['email_address'],
														'password'=>$params['password'],
														'token'=>$token
													);

									FrontFunctions::sendEmailWelcome($this->data);
								} else {

											if ( $signup_verification_enabled==1){


												/*	if($signup_verification=="sms"){		
														//send sms verification

														$signup_tpl_sms=getOptionA('signup_tpl_sms');
														if(!empty($signup_tpl_sms) && !empty($this->data['mobile_number']) ){
															$company_name=getOptionA('company_name');
															$signup_tpl_sms=smarty('first_name',$customer_params['first_name'],$signup_tpl_sms);
															$signup_tpl_sms=smarty('first_name',$customer_params['first_name'],$signup_tpl_sms);
															$signup_tpl_sms=smarty('verification_code',$verification_code,$signup_tpl_sms);
															$signup_tpl_sms=smarty('company_name',$company_name,$signup_tpl_sms);
															sendSMS($this->data['mobile_number'],$signup_tpl_sms);
														}

													} else {			*/						
														//send email verification
														FrontFunctions::sendEmailSignVerification($this->data,$verification_code);
														$this->msg=t("send email verification");
											//		}

													$this->details=array(
														'username'=>$customer_params['email_address'],
														'password'=>$params['password'],
														'token'=>$token
													);

											//		$this->msg=t("Code config send email verification");
				// 								$this->details=Yii::app()->createUrl('front/verification',array(
				// 								  'hash'=>$token,
				// 								  'type'=>$signup_verification
				// 								));

											} else {
												$db->updateData("{{customer}}",array(
													'status'=>'active'
												),'customer_id',$customer_id);
												

				// 								$this->details=Yii::app()->createUrl('front/signupty',array(
				// 								  'hash'=>$token
				// 								));
												$this->details=array(
														'username'=>$customer_params['email_address'],
														'password'=>$params['password'],
														'token'=>$token
													);

												FrontFunctions::sendEmailWelcome($this->data);
												$this->msg=t("Welcome karterord");
											}
								}

							} else $this->msg=Driver::t("failed cannot insert record driver");
						} else $this->msg=Driver::t("failed cannot insert record team");
				} else $this->msg=t("Something went wrong during processing your request");
			} else {
				$this->msg=t("Email address already exist in our records");
				
			}
			
// 		} else $this->msg=t("Plan id is missing");
		$this->output();
	
	}
	
	public function ResendCode($token)
	{
		
		if ($res=FrontFunctions::getCustomerByToken($token)){
			
			$verification_code=$res['verification_code'];
			// if($this->data['verification_type']=="mail"){				
			// 	FrontFunctions::sendEmailSignVerification($res,$verification_code);
			// 	$this->code=1;
			// 	$this->msg=t("We have sent your verification code to your email");
			// } else if ($this->data['verification_type']=="sms") {
				
				$signup_tpl_sms=getOptionA('signup_tpl_sms');
				if(!empty($signup_tpl_sms) && !empty($res['mobile_number']) ){
					$company_name=getOptionA('company_name');
					$signup_tpl_sms=smarty('first_name',$res['first_name'],$signup_tpl_sms);
					$signup_tpl_sms=smarty('first_name',$res['first_name'],$signup_tpl_sms);
					$signup_tpl_sms=smarty('verification_code',$verification_code,$signup_tpl_sms);
					$signup_tpl_sms=smarty('company_name',$company_name,$signup_tpl_sms);
					sendSMS($res['mobile_number'],$signup_tpl_sms);					
					$this->code=1;
				    $this->msg=t("We have sent your verification code to your mobile");
				} else $this->msg=t("SMS template not available");
				
			// } else $this->msg=t("Invalid verification type");
		} else $this->msg=t("hash not found");
		$this->jsonResponse();
	}

	
		public function setGenetalSettingsDefault($data)
		{
			updateOption('drv_default_location', $data['country_code'],$data['customer_id']);
			updateOption('drv_map_style','', $data['customer_id']);
			updateOption('drv_delivery_time','',$data['customer_id']);
			if(!empty($data['country_code'])){
				$country_list=require_once('CountryCode.php');	
					 $country_name='';
					 if(array_key_exists($data['country_code'],(array)$country_list)){
							 $country_name=$country_list[$data['country_code']];	   
					 } else $country_name=$this->data['country_code'];	       
					 if ( $res=Driver::addressToLatLong($country_name))	{	       	
							 updateOption("drv_default_location_lat",$res['lat'], $data['customer_id'] ); 
							 updateOption("drv_default_location_lng",$res['long'], $data['customer_id'] ); 	       	
					 } 
			 }
			updateOption('driver_send_push_to_online','',$data['customer_id']);
			updateOption('driver_active_products_orden','',$data['customer_id']);
			updateOption('driver_include_offline_driver_map','1',$data['customer_id']);
			updateOption('driver_disabled_auto_refresh','',$data['customer_id']);
			updateOption('driver_disabled_contacts_task','1',$data['customer_id']);
			updateOption('driver_enabled_notes','1',$data['customer_id']);
			updateOption('driver_enabled_signature','1',$data['customer_id']);
			updateOption('driver_enabled_photo','1',$data['customer_id']);
			updateOption('driver_device_vibration','1',$data['customer_id']);
			updateOption('driver_company_logo','avatar.png',$data['customer_id']);
			updateOption('calendar_language',isset($data['language'])?$data['language']:'US',$data['customer_id']);
			updateOption('driver_tracking_options','1',$data['customer_id']);
			updateOption('enabled_critical_task','1',$data['customer_id']);
			updateOption('critical_minutes','',$data['customer_id']);
			updateOption('agents_record_track_Location','1',$data['customer_id']);
			updateOption('map_hide_pickup','',$data['customer_id']);
			updateOption('map_hide_delivery','',$data['customer_id']);
			updateOption('map_hide_success_task','',$data['customer_id']);
			updateOption('customer_timezone','',$data['customer_id']);
			updateOption('app_enabled_resize_pic','1',$data['customer_id']);
			updateOption('app_resize_width','500',$data['customer_id']);
			updateOption('app_resize_height','500',$data['customer_id']);
			updateOption('app_disabled_bg_tracking','1',$data['customer_id']);
			updateOption('app_track_interval','',$data['customer_id']);
			updateOption('driver_auto_assign_type','send_to_all',$data['customer_id']);
			updateOption('driver_assign_request_expire','',$data['customer_id']);
			updateOption('driver_enabled_auto_assign','1',$data['customer_id']);
			updateOption('driver_include_offline_driver','1',$data['customer_id']);
			updateOption('driver_autoassign_notify_email','soporte@karterord.com',$data['customer_id']);
			updateOption('driver_request_expire','',$data['customer_id']);
			updateOption('driver_assign_radius','',$data['customer_id']);
			updateOption('driver_enabled_sort_tasks','1',$data['customer_id']);
			updateOption('driver_enabled_auto_re_assign','1',$data['customer_id']);
			updateOption('driver_autoreassign_num_days','7',$data['customer_id']);
			updateOption('DELIVERY_REQUEST_RECEIVED_SMS','1',$data['customer_id']);
			updateOption('DELIVERY_REQUEST_RECEIVED_EMAIL','1',$data['customer_id']);
			updateOption('DELIVERY_DRIVER_STARTED_SMS','1',$data['customer_id']);
			updateOption('DELIVERY_DRIVER_STARTED_EMAIL','1',$data['customer_id']);
			updateOption('DELIVERY_DRIVER_ARRIVED_SMS','1',$data['customer_id']);
			updateOption('DELIVERY_DRIVER_ARRIVED_EMAIL','1',$data['customer_id']);
			updateOption('PICKUP_REQUEST_RECEIVED_SMS','1',$data['customer_id']);
			updateOption('PICKUP_REQUEST_RECEIVED_EMAIL','1',$data['customer_id']);
			updateOption('PICKUP_DRIVER_STARTED_SMS','1',$data['customer_id']);
			updateOption('PICKUP_DRIVER_STARTED_EMAIL','1',$data['customer_id']);
			updateOption('PICKUP_DRIVER_ARRIVED_SMS','1',$data['customer_id']);
			updateOption('PICKUP_DRIVER_ARRIVED_EMAIL','1',$data['customer_id']);
			updateOption('ASSIGN_TASK_PUSH','1',$data['customer_id']);
			updateOption('ASSIGN_TASK_EMAIL','1',$data['customer_id']);
			updateOption('UPDATE_TASK_PUSH','1',$data['customer_id']);
			updateOption('FAILED_AUTO_ASSIGN_EMAIL','1',$data['customer_id']);
		}
    
    public function actionForgotPassword()
    {
			if (!empty($this->data['username'])) 
				  $this->data['email'] = $this->data['username'];
			
    	if (empty($this->data['email'])){
    		$this->msg=self::t("Email address is required");
    		$this->output();
    		Yii::app()->end();
    	}
			
			
    	$db=new DbExt;    	
    	if ( $res=Driver::driverForgotPassword($this->data['email'])){
					if ($res['status']=='active')
					{
						$driver_id=$res['driver_id'];    		
						$code=Driver::generateRandomNumber(5);
						$params=array('forgot_pass_code'=>$code);
						if($db->updateData('{{driver}}',$params,'driver_id',$driver_id)){
							$this->code=1;
							$this->msg=self::t("We have send the a password change code to your email");

							$tpl=EmailTemplate::forgotPasswordRequest();
							$tpl=smarty('first_name',$res['first_name'],$tpl);
							$tpl=smarty('code',$code,$tpl);
							$subject='Forgot Password';
							if ( sendEmail($res['email'],'',$subject,$tpl)){
								$this->details="send email ok";
							} else $this->msg="send email failed";

						} else $this->msg=self::t("Something went wrong please try again later");
					} else $this->msg=self::t("Error... ") . Driver::driverStatusLoginPretty($res['status']);
    	} else $this->msg=self::t("Email address not found");
    	$this->output();
    }
    
    public function actionChangePassword()
    {    	
    	$Validator=new Validator;
			
				if (!isset($this->data['email_address'])) {
					$email=$this->data['email'];
					$req=array(
						'email'=>self::t("Email address is required"),
						'code'=>self::t("Code is required"),
						'newpass'=>self::t("New Password is required")
					);
				} else {
					$email=$this->data['email_address'];
					$req=array(
						'email_address'=>self::t("Email address is required"),
						'code'=>self::t("Code is required"),
						'newpass'=>self::t("New Password is required")
					);
				}
				
    	$Validator->required($req,$this->data);
    	if ( $Validator->validate()){
				
    		if ( $res=Driver::driverForgotPassword($email)){
					if ($res['status']=='active')
					{
							if ( $res['forgot_pass_code']==$this->data['code']){
								$params=array( 
									'password'=>md5($this->data['newpass']),
									'date_modified'=>AdminFunctions::dateNow(),
									'forgot_pass_code'=>Driver::generateRandomNumber(5)
								 );
								$db=new DbExt;    				
								if ( $db->updateData("{{driver}}",$params,'driver_id',$res['driver_id'])){
										$this->code=1;
										$this->msg=self::t("Password successfully changed");
								} else $this->msg=self::t("Something went wrong please try again later");    				
							} else $this->msg=self::t("Invalid password code");
					} else $this->msg=self::t("Error... ") . Driver::driverStatusLoginPretty($res['status']);
    		} else $this->msg=self::t("Email address not found");
    	} else $this->msg=Driver::parseValidatorError($Validator->getError());		
    	$this->output();
    }
    
    public function actionChangeDutyStatus()
    {    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	$driver_id=$token['driver_id'];
    	    	
    	Driver::setCustomerTimezone( $token['customer_id'] );    	
    	
    	$params=array(
    	  'on_duty'=>isset($this->data['onduty'])?$this->data['onduty']:2,
    	  'last_online'=>strtotime("now"),
    	  'last_onduty'=>strtotime("now"),
    	  'last_login'=>AdminFunctions::dateNow()
    	);
    	if ( $this->data['onduty']==2){    		
    		$tracking_type=Driver::getTrackingOptions( $token['customer_id'] );
    	    if ($tracking_type==2){
    	        $params['last_online']=strtotime("-35 minutes");
    	    } else $params['last_online']=strtotime("-20 minutes");
    	    
    	    $params['is_online']=2;
    	    
    	} else $params['is_online']=1;

    	$up =Yii::app()->db->createCommand()->update("{{driver}}",$params,
  	    'driver_id=:driver_id',
	  	    array(
	  	      ':driver_id'=>$driver_id
	  	    )
  	    );
  	        	    	
    	if($up){
    		$this->code=1;
    		$this->msg="OK";
    		$this->details=$this->data['onduty'];
    	} else $this->msg=self::t("Something went wrong please try again later");   
    	$this->output();
    }
    
    // public function actionGetTaskByDateM()
    // {    	
    // 	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    // 		$this->msg=self::t("Token not valid");
    // 		$this->output();
    // 		Yii::app()->end();
    // 	} 
    // 	$driver_id=$token['driver_id'];
 	//  	  $customer_id=$token['customer_id'];    	
    	
    // 	Driver::setCustomerTimezone( $token['customer_id'] );
    	
    // 	if (isset($this->data['onduty'])){
    // 		if ($this->data['onduty']==1){    			
    // 	        Driver::updateLastOnline($driver_id);
    // 		}
    // 	}
    	
    // 	$task_type = isset($this->data['task_type'])?$this->data['task_type']:'pending';
    // 	if(empty($task_type)){
    // 		$task_type='pending';
    // 	}    	
			
	// 		$lat = isset($this->data['lat'])?$this->data['lat']:'';
	// 		$lng = isset($this->data['lng'])?$this->data['lng']:'';
			
    // 	//if ( $res=Driver::getTaskByDriverID($driver_id,$this->data['date'])){
    // 	if ( $res=Driver::getTaskByDriverIDWithAssigment($driver_id,$this->data['date'],$lat,$lng,getOptionA('app_default_lang'),$task_type,getOption($customer_id,'driver_enabled_sort_tasks'))){
    // 		$this->code=1;
    // 		$this->msg="OK";
    // 		$data=array();
    // 		foreach ($res as $val) {
					
    // 			$val['delivery_time']=Yii::app()->functions->timeFormat($val['delivery_date'],true);
    // 			$val['status_raw']=$val['status'];
    // 			$val['status']=self::t($val['status']);    			
    // 			$val['trans_type_raw']=$val['trans_type'];
    // 			$val['trans_type']=self::t($val['trans_type']);
	// 			// $val['distancia0']=getOption($customer_id,'driver_enabled_sort_tasks');
    // 			$data[]=$val;
    // 		}
    // 		$this->details=$data;
    // 	} else $this->msg=self::t("No task for the day");
    // 	$this->output();
    // }
	
	public function actionGetTaskByDateM()
    {    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	$driver_id=$token['driver_id'];
 	 	  $customer_id=$token['customer_id'];    	
    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	
    	if (isset($this->data['onduty'])){
    		if ($this->data['onduty']==1){    			
    	        Driver::updateLastOnline($driver_id);
    		}
    	}
    	
    	$task_type = isset($this->data['task_type'])?$this->data['task_type']:'';
    	$lat = isset($this->data['lat'])?$this->data['lat']:'';
			$lng = isset($this->data['lng'])?$this->data['lng']:'';
    	
    	//if ( $res=Driver::getTaskByDriverID($driver_id,$this->data['date'])){
   		if ( $res=Driver::getTaskByDriverIDWithAssigmentM($driver_id,$this->data['date'],$lat,$lng,getOptionA('app_default_lang'),$task_type,getOption($customer_id,'driver_enabled_sort_tasks'))){
    		$this->code=1;
    		$this->msg="OK";
    		$data=array();
    		foreach ($res as $val) {
    			$val['delivery_time']=Yii::app()->functions->timeFormat($val['delivery_date'],true);
    			$val['status_raw']=$val['status'];
    			$val['status']=self::t($val['status']);    			
    			$val['trans_type_raw']=$val['trans_type'];
    			$val['trans_type']=self::t($val['trans_type']);
				// $val['distancia0']=getOption($customer_id,'driver_enabled_sort_tasks');
    			$data[]=$val;
    		}
    		$this->details=$data;
    	} else $this->msg=self::t("No task for the day");
    	$this->output();
	}
	
	public function actionGetTaskByIDM()
    {    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	$driver_id=$token['driver_id'];
		  $customer_id=$token['customer_id'];  
		    	
    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	
    	if (isset($this->data['onduty'])){
    		if ($this->data['onduty']==1){    			
    	        Driver::updateLastOnline($driver_id);
    		}
    	}
    	
    	$task_type = isset($this->data['task_type'])?$this->data['task_type']:'';
    	$lat = isset($this->data['lat'])?$this->data['lat']:'';
			$lng = isset($this->data['lng'])?$this->data['lng']:'';
    	
    	//if ( $res=Driver::getTaskByDriverID($driver_id,$this->data['date'])){
   		if ( $res=Driver::getTaskByDriverTaskIDtM($this->data['task_id'],$this->data['date'],$lat,$lng,getOptionA('app_default_lang'),$task_type,getOption($customer_id,'driver_enabled_sort_tasks'))){
    		$this->code=1;
    		$this->msg="OK";
    		$data=array();
    		foreach ($res as $val) {
    			$val['delivery_time']=Yii::app()->functions->timeFormat($val['delivery_date'],true);
    			$val['status_raw']=$val['status'];
    			$val['status']=self::t($val['status']);    			
    			$val['trans_type_raw']=$val['trans_type'];
    			$val['trans_type']=self::t($val['trans_type']);
				// $val['distancia0']=getOption($customer_id,'driver_enabled_sort_tasks');
    			$data[]=$val;
    		}
    		$this->details=$data;
    	} else $this->msg=self::t("No task for the day");
    	$this->output();
    }
	
	public function actionUpdateOnlineM(){
		if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id'];    	
    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	
    	if (isset($this->data['onduty'])){
    		if ($this->data['onduty']==1){    			
    	        Driver::updateLastOnline($driver_id);
    		}
		}
		$this->output();
	}

    public function actionGetTaskByDate()
    {    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id'];    	
    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	
    	if (isset($this->data['onduty'])){
    		if ($this->data['onduty']==1){    			
    	        Driver::updateLastOnline($driver_id);
    		}
    	}
    	
    	$task_type = isset($this->data['task_type'])?$this->data['task_type']:'pending';
    	if(empty($task_type)){
    		$task_type='pending';
    	}    	
    	    	
    	if ( $res=Driver::getTaskByDriverIDWithAssigment($driver_id,$this->data['date'],$task_type)){
    		$this->code=1;
    		$this->msg="OK";
    		$data=array();
    		foreach ($res as $val) {
    			$val['delivery_time']=Yii::app()->functions->timeFormat($val['delivery_date'],true);
    			$val['status_raw']=$val['status'];
    			$val['status']=self::t($val['status']);    			
    			$val['trans_type_raw']=$val['trans_type'];
    			$val['trans_type']=self::t($val['trans_type']);    			
    			$data[]=$val;
    		}
    		$this->details=$data;
    	} else {    		
    		$task_type = isset($this->data['task_type'])?$this->data['task_type']:'';    		
    		$this->msg = $task_type=="completed"? self::t("No task completed") :self::t("No task for the day");
    	}
    	$this->output();
    }
    public function actionviewTaskDescription()
    {
    	$this->actionTaskDetails();
    }
    public function actionTaskDetails()
    {    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}   
    	    	
    	$customer_id=$token['customer_id'];    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	
    	if (isset($this->data['task_id'])){
    		if ( $res=Driver::getTaskId($this->data['task_id']) ){
    			
    			//check task belong to current driver    			    			
    			if ( $res['status']!="unassigned"){
	    			$driver_id=$token['driver_id'];
	    			if ($driver_id!=$res['driver_id']){
	    				$this->msg=Driver::t("Sorry but this task is already been assigned to others");
	    				$this->output();
	    				Yii::app()->end();
	    			}    			
    			}
    			
    			$this->code=1;
    			$this->msg=self::t("Task").":".$this->data['task_id'];
    			
    			$res['delivery_time']=Yii::app()->functions->timeFormat($res['delivery_date'],true);    			
    			$res['status_raw']=$res['status'];
    			$res['status']=self::t($res['status']);    			
    			$res['trans_type_raw']=$res['trans_type'];
    			$res['trans_type']=self::t($res['trans_type']);
    			
    			$res['history']=Driver::getDriverTaskHistory($this->data['task_id']);
    			
    			/*get signature if any*/
    			$res['customer_signature_url']='';
    			if (!empty($res['customer_signature'])){
    				$res['customer_signature_url']=Driver::uploadURL()."/".$res['customer_signature'];
    				if (!file_exists(Driver::uploadPath()."/".$res['customer_signature'])){
    					$res['customer_signature_url']='';
    				}
    			}
    			
				$res['driver_enabled_sort_tasks']=getOption( $customer_id , 'driver_enabled_sort_tasks');
    			$res['driver_enabled_notes']=getOption( $customer_id , 'driver_enabled_notes');
    			$res['driver_enabled_signature']=getOption( $customer_id , 'driver_enabled_signature');
    			$res['driver_enabled_photo']=getOption( $customer_id , 'driver_enabled_photo');
    			$res['history_notes']['total']=Driver::getNotesTotal( $this->data['task_id'] );
    			$res['task_photo']['total']=Driver::getTaskPhotoTotal( $this->data['task_id'] );
    			
    			$res['map_icons']=array(
    			  'driver'=>websiteUrl()."/assets/images/car.png",
				  'customer'=>websiteUrl()."/assets/images/racing-flag.png",
				  'merchant'=>websiteUrl()."/assets/images/restaurant-pin-32.png",
    			);
    			
    			$app_enabled_resize_pic=getOption($customer_id,'app_enabled_resize_pic');
    			$app_resize_width=getOption($customer_id,'app_resize_width');
    			$app_resize_height=getOption($customer_id,'app_resize_height');
    			
    			if ( $app_resize_width<=0){
    				$app_enabled_resize_pic=2;
    			}
    			if ( $app_resize_height<=0){
    				$app_enabled_resize_pic=2;
    			}
    			if (empty($app_enabled_resize_pic)){
    				$app_enabled_resize_pic=2;
    			}
    			
    			$res['resize_picture']=array(
    			   'app_enabled_resize_pic'=>$app_enabled_resize_pic,
    			   'app_resize_width'=>$app_resize_width,
    			   'app_resize_height'=>$app_resize_height,
    			);
    			    			    					
    			$this->details=$res;
    		} else $this->msg=self::t("Task not found");
    	} else $this->msg=self::t("Task id is missing");
    	$this->output();
    }
	
    public function actionChangeTaskStatus()
    {
    	
    	if(isset($_GET['debug'])){
    	   dump($this->data);
    	}
    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id'];    	
    	$team_id=$token['team_id']; 
    	$driver_name=$token['first_name'] ." " .$token['last_name'];    
    	
    	Driver::setCustomerTimezone( $token['customer_id'] );	
    	
    	$db=new DbExt;	
    	
    	if (isset($this->data['status_raw']) && isset($this->data['task_id'])){
    		
    		$task_id=$this->data['task_id'];
    		$task_info=Driver::getTaskId($task_id);
    		if(!$task_info){
    			$this->msg=self::t("Task not found");
    			$this->output();
    			Yii::app()->end();
    		}    		
    		
    		$params_history=array();    		
    		$params_history['ip_address']=$_SERVER['REMOTE_ADDR'];
    	    $params_history['date_created']=AdminFunctions::dateNow();
    	    $params_history['task_id']=$task_id;    	    
    	    $params_history['driver_id']=$driver_id;        	    
    	    $params_history['driver_location_lat']=isset($this->data['location_lat'])?$this->data['location_lat']:'';
    	    $params_history['driver_location_lng']=isset($this->data['location_lng'])?$this->data['location_lng']:'';
    	    
    				
    		
    		switch ($this->data['status_raw']) {
    			
    			case "failed":
    			case "cancelled":    	
    			   $params=array('status'=>$this->data['status_raw']);    				
    				// update task id
    				$db->updateData("{{driver_task}}",$params,'task_id',$task_id);
    				
    				$remarks=Driver::driverStatusPretty($driver_name,$this->data['status_raw']);    				
    				$params_history['status']=$this->data['status_raw'];
    				$params_history['remarks']=$remarks; 			    				
    				$params_history['reason']=isset($this->data['reason'])?$this->data['reason']:'' ; 
    				// insert history    				
    				$db->insertData("{{task_history}}",$params_history);
    				
    				$this->code=1;
    				$this->msg="OK";
    				$this->details=array(
    				  'task_id'=>$this->data['task_id'],
    				  'status_raw'=>$params['status'],
    				  'reload_functions'=>'getTodayTask'
    				);    				
    				
    				//send notification to customer
    				if ( $task_info['trans_type']=="delivery"){    					
    				    Driver::sendNotificationCustomer('DELIVERY_FAILED',$task_info);
    				} else {
    					Driver::sendNotificationCustomer('PICKUP_FAILED',$task_info);
    				}
    							
    				break;
    				
    			case "declined":
    				
    				if ( $assigment_info=Driver::getAssignmentByDriverTaskID($driver_id,$task_id)){
    					
    					$stmt_assign="UPDATE 
    					{{driver_assignment}}
    					SET task_status='declined',
    					date_process=".Driver::q(AdminFunctions::dateNow()).",
    					ip_address=".Driver::q($_SERVER['REMOTE_ADDR'])."
    					WHERE
    					task_id=".Driver::q($task_id)."
    					AND
    					driver_id=".Driver::q($driver_id)."
    					";
    					//dump($stmt_assign);
    					$db->qry($stmt_assign);
    					
    					$this->code=1;
	    				$this->msg="OK";
	    				$this->details=array(
	    				  'task_id'=>$this->data['task_id'],
	    				  'status_raw'=>'declined',
	    				  'reload_functions'=>'getTodayTask'
	    				);    				
	    				
    				} else {
	    				$params=array('status'=>"declined");    
	    				//dump($params);
	    				// update task id
	    				$db->updateData("{{driver_task}}",$params,'task_id',$task_id);
	    				
	    				$remarks=Driver::driverStatusPretty($driver_name,'declined');    				
	    				$params_history['status']='declined';
	    				$params_history['remarks']=$remarks;  
	    				$params_history['reason'] ='';  				    				
	    				// insert history    				
	    				$db->insertData("{{task_history}}",$params_history);
	    				
	    				$this->code=1;
	    				$this->msg="OK";
	    				$this->details=array(
	    				  'task_id'=>$this->data['task_id'],
	    				  'status_raw'=>$params['status'],
	    				  'reload_functions'=>'getTodayTask'
	    				);    				
	    				
	    				//send email to admin or merchant
    				}
    				
    				break;
    				
    				
    			case "acknowledged":    		
    			
    			    // double check if someone has already the accept task   			    
    			    if($task_info['status']!="unassigned"){        			    	
    			    	if ( $task_info['driver_id']!=$driver_id){			    	
    			           $this->msg=Driver::t("Sorry but this task is already been assigned to others");
    			           $this->output();
    			    	   Yii::app()->end();
    			    	}
    			    }
    			    
    				$params=array(
    				  'driver_id'=>$driver_id,
    				  'status'=>"acknowledged",
    				  'team_id'=>$team_id
    				);    	
    				
    				// update task id    				
    				$db->updateData("{{driver_task}}",$params,'task_id',$task_id);
    				
    				$remarks=Driver::driverStatusPretty($driver_name,'acknowledged');
    				$params_history['status']='acknowledged';
    				$params_history['remarks']=$remarks; 
    				$params_history['reason'] ='';
    				// insert history     				
    				$db->insertData("{{task_history}}",$params_history);
    				
    				$this->code=1;
    				$this->msg="OK";
    				$this->details=array(
    				  'task_id'=>$this->data['task_id'],
    				  'status_raw'=>$params['status'],
    				  'reload_functions'=>'TaskDetails'
    				);    				
    				
    				//update driver_assignment
    				$stmt_assign="UPDATE
    				{{driver_assignment}}
    				SET task_status='acknowledged'
    				WHERE task_id=".Driver::q($task_id)."
    				";
    				$db->qry($stmt_assign);
    				
    				//send notification to customer
    				if ( $task_info['trans_type']=="delivery"){  
    				   Driver::sendNotificationCustomer('DELIVERY_REQUEST_RECEIVED',$task_info);
    				} else {
    				   Driver::sendNotificationCustomer('PICKUP_REQUEST_RECEIVED',$task_info);
    				}
    				
    				break;
    				
    			case "started":	
    			    $params=array('status'=>"started");
    			    $db->updateData("{{driver_task}}",$params,'task_id',$task_id);
    				// update task id
    				
    				$remarks=Driver::driverStatusPretty($driver_name,'started');   
    				$params_history['status']='started';
    				$params_history['remarks']=$remarks;    	
    				$params_history['reason'] ='';			
    				// insert history
    				$db->insertData("{{task_history}}",$params_history);
    				
    				$this->code=1;
    				$this->msg="OK";
    				$this->details=array(
    				  'task_id'=>$this->data['task_id'],
    				  'status_raw'=>$params['status'],
    				  'reload_functions'=>'TaskDetails'
    				);    		
    				
    				//send notification to customer
    				if ( $task_info['trans_type']=="delivery"){  
    				    Driver::sendNotificationCustomer('DELIVERY_DRIVER_STARTED',$task_info);
    				} else {
    					Driver::sendNotificationCustomer('PICKUP_DRIVER_STARTED',$task_info);
    				}
    						
    				break;    			   
    		
    			case "inprogress":
    				 $params=array('status'=>"inprogress");
    				 $db->updateData("{{driver_task}}",$params,'task_id',$task_id);
    				// update task id
    				
    				$remarks=Driver::driverStatusPretty($driver_name,'inprogress');    				
    				$params_history['status']='inprogress';
    				$params_history['remarks']=$remarks;    	
    				$params_history['reason'] ='';						
    				// insert history
    				$db->insertData("{{task_history}}",$params_history);
    				
    				$this->code=1;
    				$this->msg="OK";
    				$this->details=array(
    				  'task_id'=>$this->data['task_id'],
    				  'status_raw'=>$params['status'],
    				  'reload_functions'=>'TaskDetails'
    				);    			
    				
    				//send notification to customer
    				if ( $task_info['trans_type']=="delivery"){  
    				   Driver::sendNotificationCustomer('DELIVERY_DRIVER_ARRIVED',$task_info);
    				} else {
    				   Driver::sendNotificationCustomer('PICKUP_DRIVER_ARRIVED',$task_info);
    				}
    				
    				break;
    				
    			case "successful":	    			   
    			    $params=array('status'=>"successful");
    			    $db->updateData("{{driver_task}}",$params,'task_id',$task_id);
    				// update task id
    				
    				$remarks=Driver::driverStatusPretty($driver_name,'successful');    				
    				$params_history['status']='successful';
    				$params_history['remarks']=$remarks;    
    				$params_history['reason'] ='';				
    				// insert history
    				$db->insertData("{{task_history}}",$params_history);
    				
    				$this->code=1;
    				$this->msg="OK";
    				$this->details=array(
    				  'task_id'=>$this->data['task_id'],
    				  'status_raw'=>$params['status'],
    				  'reload_functions'=>'getTodayTask'
    				);    			
    				
    				//send notification to customer
    				if ( $task_info['trans_type']=="delivery"){  
    				    Driver::sendNotificationCustomer('DELIVERY_SUCCESSFUL',$task_info);
    				} else {
    					Driver::sendNotificationCustomer('PICKUP_SUCCESSFUL',$task_info);
    				}
    				
    				break;
    				   
    			default:
    				$this->msg=self::t("Missing status");
    				break;
    		}
    	} else $this->msg=self::t("Missing parameters");
    	
    	$this->output();
    }
    
    public function actionAddSignatureToTask()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id'];    	
    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
			
	  	if ( isset($this->data['image'])){
    		    		
    		if ($this->data['image']=="image/jsignature;base30,"){
    			$this->msg=self::t("Signature is required");
    			$this->output();
    		    Yii::app()->end();
    		}
    		
	    	$path_to_upload=Yii::getPathOfAlias('webroot')."/upload";      	
	    	if (!file_exists($path_to_upload)){
	    		if (!@mkdir($path_to_upload,0777)){           	    
	    			$this->msg=self::t("Failed cannot create folder"." ".$path_to_upload);
           	        Yii::app()->end();
                }		    
	    	}
				
	    	
	    	$filename="signature_".$this->data['task_id'] . "-" . Driver::generateRandomNumber(10) .".png";
				
	    	//$filename="signature_".$this->data['task_id'] . "-.png";
	    	
	    	/*$img = $this->data['image'];
	    	$img = str_replace('data:image/png;base64,', '', $img);
	        $img = str_replace(' ', '+', $img);
	        $data = base64_decode($img);
	        @file_put_contents($path_to_upload."/$filename", $data);*/
	    	
	    	
	    	$img = $this->data['image'];	   	    	
	    	Driver::base30_to_jpeg($img, $path_to_upload."/$filename");	    	
	        	        
	        $params=array(
	          'customer_signature'=>$filename,
	          'date_modified'=>AdminFunctions::dateNow(),
	          'ip_address'=>$_SERVER['REMOTE_ADDR']
	        );
	        
	        $task_id=$this->data['task_id'];	  
	        $driver_name=$token['first_name'] ." " .$token['last_name'];         

	        $db=new DbExt;		        
	        
	        $task_id=$this->data['task_id'];
    		$task_info=Driver::getTaskId($task_id);
    		if(!$task_info){
    			$this->msg=self::t("Task not found");
    			$this->output();
    			Yii::app()->end();
    		}    		
	        
	        if ( $db->updateData("{{driver_task}}",$params,'task_id',$task_id)){
		        $this->code=1;
		        $this->msg="Successful";      
		        $this->details=$this->data['task_id'];	
		        
		        $remarks=Driver::driverStatusPretty($driver_name,'sign');  
		        $params_history=array(
		           'status'=>'sign',
		           'remarks'=>$remarks,
		           'date_created'=>AdminFunctions::dateNow(),
		           'ip_address'=>$_SERVER['REMOTE_ADDR'],
		           'task_id'=>$task_id,
		           'customer_signature'=>$filename ,		           
		           'driver_id'=>$driver_id,
		           'driver_location_lat'=>isset($this->data['location_lat'])?$this->data['location_lat']:'',
		           'driver_location_lng'=>isset($this->data['location_lng'])?$this->data['location_lng']:'',
		           'reason'=>'',
		           'receive_by'=>isset($this->data['receive_by'])?$this->data['receive_by']:'',
		           'signature_base30'=>$this->data['image']
		        );
		        
		        if ( $this->data['signature_id']>0){
		        	$db->updateData("{{task_history}}",$params_history,'id',$this->data['signature_id']);
		        } else $db->insertData("{{task_history}}",$params_history);       
		        		        
 			    $task_info['signature_link']=websiteUrl()."/upload/$filename";		
 			    if ( $task_info['trans_type']=="delivery"){ 
 				   Driver::sendCustomerNotification('DELIVERY_SIGNATURE',$task_info);
 			    } else Driver::sendCustomerNotification('PICKUP_SIGNATURE',$task_info);	           
		        	       
	        } else $this->msg=self::t("Something went wrong please try again later");
	        
    	} else $this->msg=self::t("Signature is required");
    	$this->output();     
    }
	
	public function actionAddSignatureToTaskM()
    {
		
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id'];    	
    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
		
				if (isset($_FILES['image'])) {
					header('Access-Control-Allow-Origin: *');
				
			    $name = $_FILES['image']['name'];
					$filename=$name."-".$this->data['task_id'] . "-" . Driver::generateRandomNumber(10) .".png";
					$path_to_upload=Driver::driverUploadPathSig();
					$target_dir =$path_to_upload;
				 //	$filename="signature_".$name . "-" . Driver::generateRandomNumber(10) .".png";
				
					$target_file = $target_dir . $filename;
					
					 // Select file type
					$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

						// Valid file extensions
					$extensions_arr = array("jpg","jpeg","png");

					
					// Check extension
					if( in_array($imageFileType,$extensions_arr) ){
						
					if (!file_exists($path_to_upload)){
						if (!@mkdir($path_to_upload,0777)){           	    
							$this->msg=self::t("Failed cannot create folder"." ".$path_to_upload);
											Yii::app()->end();
									}		    
					}
						
					if(@move_uploaded_file($_FILES["image"]["tmp_name"], "$path_to_upload/".$filename)){
						
						//$img = $image;	
// 	    		Driver::base30_to_jpeg($img, $path_to_upload."/$filename");	 
						
						  $params=array(
	          'customer_signature'=>$filename,
	          'date_modified'=>AdminFunctions::dateNow(),
	          'ip_address'=>$_SERVER['REMOTE_ADDR']
	        );
	        
	        $task_id=$this->data['task_id'];	  
	        $driver_name=$token['first_name'] ." " .$token['last_name'];         

	        $db=new DbExt;		        
	        
	        $task_id=$this->data['task_id'];
					$task_info=Driver::getTaskId($task_id);
					if(!$task_info){
						$this->msg=self::t("Task not found");
						$this->output();
						Yii::app()->end();
					}    		
	        
	        if ( $db->updateData("{{driver_task}}",$params,'task_id',$task_id)){
		        $this->code=1;
		        $this->msg="Successful";      
		        $this->details=$this->data['task_id'];	
		        
		        $remarks=Driver::driverStatusPretty($driver_name,'sign');  
		        $params_history=array(
		           'status'=>'sign',
		           'remarks'=>$remarks,
		           'date_created'=>AdminFunctions::dateNow(),
		           'ip_address'=>$_SERVER['REMOTE_ADDR'],
		           'task_id'=>$task_id,
		           'customer_signature'=>$filename ,		           
		           'driver_id'=>$driver_id,
		           'driver_location_lat'=>isset($token['location_lat'])?$token['location_lat']:'',
		           'driver_location_lng'=>isset($token['location_lng'])?$token['location_lng']:'',
		           'reason'=>'',
		           'receive_by'=>isset($this->data['receive_by'])?$this->data['receive_by']:''
		        );
				
				$js_code = 'console.log driver_task sign(' . json_encode($params_history, JSON_HEX_TAG) . ');';
				error_log($js_code);

		        if ( $this->data['signature_id']>0){
					$js_code = 'console.log update task_history sign(' . json_encode($params_history, JSON_HEX_TAG) . ');';
				error_log($js_code);
					$db->updateData("{{task_history}}",$params_history,'id',$this->data['signature_id']);
		        } else {
					$js_code = 'console.log insert task_history sign(' . json_encode($params_history, JSON_HEX_TAG) . ');';
				error_log($js_code);
					 $db->insertData("{{task_history}}",$params_history);       }
		        		        
 			    $task_info['signature_link']=websiteUrl()."/upload/$filename";		
 			    if ( $task_info['trans_type']=="delivery"){ 
 				   Driver::sendCustomerNotification('DELIVERY_SIGNATURE',$task_info);
 			    } else Driver::sendCustomerNotification('PICKUP_SIGNATURE',$task_info);	           
		        	       
	        } else $this->msg=self::t("Something went wrong please try again later");
						
						// Upload file
// 						move_uploaded_file($_FILES['image']['tmp_name'],$target_dir.$name);
 					} else $this->msg=self::t("Cannot upload photo");
					} else {
						$this->msg=self::t("extensions error");
							$this->output();
								Yii::app()->end();
					}
					
				
    		
	    	    
			
	    	//$filename="signature_".$this->data['task_id'] . "-.png";
	    	
	    	/*$img = $this->data['image'];
	      	$img = str_replace('data:image/png;base64,', '', $img);
	        $img = str_replace(' ', '+', $img);
	        $data = base64_decode($img);
	        @file_put_contents($path_to_upload."/$filename", $data);*/
	  
			
					
					   	
	        	        
	      
			
    	} else $this->msg=self::t("Signature is required");
    	$this->output();     
    }
    
    public function actionCalendarTask()
    {    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id'];       	
    	Driver::setCustomerTimezone( $token['customer_id'] );	
    	
    	if (isset($this->data['start']) && isset($this->data['end'])){
    		
    		if(isset($this->data['json'])){
	    		$this->data['start'] = date("Y-m-d",strtotime($this->data['start']));
	    		$this->data['end'] = date("Y-m-d",strtotime($this->data['end']));
	    		$start=$this->data['start'] ." 00:00:00";
    		    $end=$this->data['end'] ." 23:59:00";    		
    		} else {
    			$start=$this->data['start'];
    		    $end=$this->data['end'];
    		}
    		    		    		    		
    		$data=array();
    		    		
    		if ( $res=Driver::getDriverTaskCalendar($driver_id,$start,$end)){    			
    			 foreach ($res as $val) {  
    			 	if(isset($this->data['json'])){
    			 		$data[]=array(
    			 		  'title'=> Driver::getTotalTaskByDate($driver_id,$val['delivery_date']),
    			 		  'start'=> $val['delivery_date'],
    			 		  'end'=>$val['delivery_date'],
    			 		);
    			 	} else {
	    			 	$data[]=array(
	    			 	  'title'=> Driver::getTotalTaskByDate($driver_id,$val['delivery_date']),
	    			 	  'id'=>$val['delivery_date'],
	    			 	  'year'=>date("Y",strtotime($val['delivery_date'])),
	    			 	  'month'=>date("m",strtotime($val['delivery_date'] ." -1 months" )),
	    			 	  'day'=>date("d",strtotime($val['delivery_date'])),
	    			 	);
    			 	}
    			 }
    			 $this->code=1;
    			 $this->msg="OK";
    			 $this->details=$data;
    		}
    	} else $this->msg=self::t("Missing parameters");
    	
    	if(isset($this->data['json'])){
    		header('Access-Control-Allow-Origin: *');       	  
            header('Content-type: application/javascript;charset=utf-8');           
    		echo json_encode($data);
    	} else $this->output(); 
    }
    
    public function actionGetProfile()
    {    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id'];    	
    	$info=Driver::driverInfo($driver_id); 
    	
    	$profile_photo='';
    	if(!empty($info['profile_photo'])){
    		$profile_photo_path=Driver::driverUploadPath()."/".$info['profile_photo'];
    		if(file_exists($profile_photo_path)){
    			$profile_photo=websiteUrl()."/upload/photo/".$info['profile_photo'];
    		}
    	}
    	    	  
    	$this->code=1;
    	$this->msg="OK";
    	$this->details=array(
    	  'team_name'=>$info['team_name'],
    	  'email'=>$info['email'],
    	  'phone'=>$info['phone'],
    	  'transport_type_id'=>$info['transport_type_id'],
    	  'transport_type_id2'=>ucwords(self::t($info['transport_type_id'])),
    	  'transport_description'=>$info['transport_description'],
    	  'licence_plate'=>$info['licence_plate'],
    	  'color'=>$info['color'],
    	  'profile_photo'=>$profile_photo,
    	  'full_name'=>$info['first_name']." ".$info['last_name'],
    	  'transport_list'=>Driver::transportType()
    	);
    	$this->output();     
    }
    
    public function actionGetTransport()
    {    	
    	$this->code=1;
    	$this->code=1;
    	$this->details=Driver::transportType();
    	$this->output();     
    }
    
    public function actionUpdateProfile()
    {    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id']; 
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	
    	$Validator=new Validator;
    	$req=array(
    	  'phone'=>self::t("Phone is required")    	  
    	);
    	$Validator->required($req,$this->data);
    	if ( $Validator->validate()){
    		$params=array(
    		  'phone'=>$this->data['phone'],
    		  'date_modified'=>AdminFunctions::dateNow(),
    		  'ip_address'=>$_SERVER['REMOTE_ADDR']
    		);
    		$db=new DbExt;
    		if ( $db->updateData("{{driver}}",$params,'driver_id',$driver_id)){
    			$this->code=1;
    			$this->msg=self::t("Profile Successfully updated");
    		} else $this->msg=self::t("Something went wrong please try again later");
    	} else $this->msg=Driver::parseValidatorError($Validator->getError());
    	$this->output();     
    }
    
    public function actionUpdateVehicle()
    {    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id']; 
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	
    	$Validator=new Validator;
    	$req=array(
    	  'transport_type_id'=>self::t("Transport Type is required"),
    	  'transport_description'=>self::t("Description is required"),
    	  /*'licence_plate'=>self::t("License Plate is required"),
    	  'color'=>self::t("Color is required"),*/
    	);
    	if ( $this->data['transport_type_id']=="truck"){
    		unset($req);
    		$req=array(
    		  'transport_type_id'=>self::t("Transport Type is required")
    		);
    	}
    	$Validator->required($req,$this->data);
    	if ( $Validator->validate()){
    		$params=array(
    		  'transport_type_id'=>$this->data['transport_type_id'],
    		  'transport_description'=>$this->data['transport_description'],
    		  'licence_plate'=>isset($this->data['licence_plate'])?$this->data['licence_plate']:'',
    		  'color'=>isset($this->data['color'])?$this->data['color']:'',
    		  'date_modified'=>AdminFunctions::dateNow(),
    		  'ip_address'=>$_SERVER['REMOTE_ADDR']
    		);
    		$db=new DbExt;
    		if ( $db->updateData("{{driver}}",$params,'driver_id',$driver_id)){
    			$this->code=1;
    			$this->msg=self::t("Vehicle Info updated");
    		} else $this->msg=self::t("Something went wrong please try again later");
    	} else $this->msg=Driver::parseValidatorError($Validator->getError());
    	$this->output();     
    }
    
    public function actionProfileChangePassword()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id = (integer) $token['driver_id']; 
    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	
    	$Validator=new Validator;
    	$req=array(
    	  'current_pass'=>self::t("Current password is required"),
    	  'new_pass'=>self::t("New password is required"),
    	  'confirm_pass'=>self::t("Confirm password is required")    	  
    	);    	
    	if ( $this->data['new_pass']!=$this->data['confirm_pass']){
    		$Validator->msg[]=self::t("Confirm password does not macth with your new password");
    	}
    	
    	$Validator->required($req,$this->data);
    	if ( $Validator->validate()){
    		    		    		
    		if (!Driver::driverAppLogin($token['username'],$this->data['current_pass'])){
    			$this->msg=self::t("Current password is invalid");
    			$this->output();     
    			Yii::app()->end();
    		}    		
    		
    		if (Driver::driverAppLogin($token['username'],$this->data['new_pass'])){
    			$this->msg=self::t("New password is the same as old password");
    			$this->output();     
    			Yii::app()->end();
    		}    		
    		
    		$params=array(
    		  'password'=>md5($this->data['new_pass']),
    		  'date_modified'=>AdminFunctions::dateNow(),
    		  'ip_address'=>$_SERVER['REMOTE_ADDR']
    		);    		
    		
	        $up = Yii::app()->db->createCommand()->update("{{driver}}",$params,
	  	    'driver_id=:driver_id',
		  	    array(
		  	      ':driver_id'=>$driver_id
		  	    )
	  	    );    		
    		if($up){
    			$this->code=1;
    			$this->msg=self::t("Password Successfully Changed");
    			$this->details=$this->data['new_pass'];
    		} else $this->msg=self::t("Something went wrong please try again later");
    	} else $this->msg=Driver::parseValidatorError($Validator->getError());
    	$this->output();     
    }
    
    public function actionSettingPush()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	
    	$driver_id = (integer) $token['driver_id']; 
    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	    	
    	$params=array(
    	  'enabled_push'=>isset($this->data['enabled_push'])? (integer) $this->data['enabled_push']:0,
    	  'date_modified'=>AdminFunctions::dateNow(),
    	  'ip_address'=>$_SERVER['REMOTE_ADDR']
    	);
    	
        $up = Yii::app()->db->createCommand()->update("{{driver}}",$params,
  	    'driver_id=:driver_id',
	  	    array(
	  	      ':driver_id'=>$driver_id
	  	    )
  	    );   			
		if($up){	
			$this->code=1;
			$this->msg=self::t("Setting Saved");	
			
			$this->details = array(
			  'enabled_push'=> (integer) $params['enabled_push']
			);
					
		} else $this->msg=self::t("Something went wrong please try again later");
		$this->output();     
    }
    
    public function actionGetSettings()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id']; 
    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	
    	$lang=Driver::availableLanguages();
    	$lang='';
    	
    	$resp=array(
    	  'enabled_push'=>$token['enabled_push'],
    	  'language'=>$lang
    	);
    	$this->code=1;
    	$this->msg="OK";
    	$this->details=$resp;
    	$this->output();     
    }
    
    public function actionLanguageList()
    {
    	$final_list = array();
    	$lang=getOptionA('language_list');
    	if(!empty($lang)){
    		$lang=json_decode($lang,true);
    	}
    	if(is_array($lang) && count($lang)>=1){
    		foreach ($lang as $lng) {
    			$final_list[$lng]=$lng;
    		}
    		$this->code=1; $this->msg="OK";
    	} else $this->msg=t("No language");
    	$this->details=$final_list;    	
		$this->output();
    }
    
    public function actionGetAppSettings()
    {    	
    	
    	$translation=Driver::getMobileTranslation();    	
    	$this->code=1;
    	$this->msg="OK";
    	$this->details=array(
    	  'notification_sound_url'=>Driver::moduleUrl()."/sound/food_song.mp3",  
    	  'app_default_lang'=>getOptionA('app_default_lang'),
    	  'app_force_lang'=>getOptionA('app_force_lang'),
    	  'map_provider'=>getOptionA('map_provider'),
    	  'mapbox_access_token'=>getOptionA('mapbox_access_token'),
    	  'translation'=>$translation
    	);    	
    	
    	$token_data = array();
    	$token_data['valid_token']=false;
    	
    	$token = isset($this->data['token'])? trim($this->data['token']) :'';
    	if(!empty($token)){
    		if($resp = Driver::getDriverByTokenInfo($token)){    	
    			
    			Driver::setCustomerTimezone( $resp['customer_id'] );
    					
    			$token_data['valid_token']=true; 
    			$topic_id = $resp['customer_id'];
    			$token_data['todays_date']=Yii::app()->functions->translateDate(date("M, d"));
				$token_data['todays_date_raw']=date("Y-m-d");
				$token_data['on_duty']=$resp['on_duty'];
				$token_data['token']=$token;
				$token_data['duty_status']=$resp['on_duty'];
				$token_data['enabled_push']=(integer)$resp['enabled_push'];
				$token_data['topic_new_task']=str_replace("/topics/","",CHANNEL_TOPIC).$topic_id;
				$token_data['topic_alert']=str_replace("/topics/","",CHANNEL_TOPIC_ALERT).$topic_id;
				$token_data['location_accuracy'] = 2;
				if($resp['location_accuracy']=="high"){
					$token_data['location_accuracy'] = 1;
				}
				$token_data['device_vibration'] = getOption($resp['customer_id'],'driver_device_vibration');				
				$token_data['app_disabled_bg_tracking'] = getOption($resp['customer_id'],'app_disabled_bg_tracking');				
				$app_track_interval=getOption($resp['customer_id'],'app_track_interval');
    			if (!is_numeric($app_track_interval)){
    				$app_track_interval=60000;
    			} else $app_track_interval=$app_track_interval*1000;
    			
    			if ($app_track_interval<=0){
    				$app_track_interval=60000;
    			}				
    			$token_data['app_track_interval'] = $app_track_interval;
    			    			
    			$params=array(
	    		  'last_login'=>AdminFunctions::dateNow(),
	    		  'last_online'=>strtotime("now"),
	    		  'ip_address'=>$_SERVER['REMOTE_ADDR'],
	    		  'token'=>$token,
	    		  'device_id'=>isset($this->data['device_id'])?$this->data['device_id']:'',
	    		  'device_platform'=>isset($this->data['device_platform'])?strtolower(trim($this->data['device_platform'])) :'',
	    		  'app_version'=>isset($this->data['app_version'])?$this->data['app_version']:'',
	    		);	
	    			    			    	
	    		Yii::app()->db->createCommand()->update("{{driver}}",$params,
		  	    'driver_id=:driver_id',
			  	    array(
			  	      ':driver_id'=>$resp['driver_id']
			  	    )
		  	    );    			
    		}
    	}
    	
    	$this->details = array_merge($this->details,$token_data);
    	    	
    	$this->output();
    }
    
    public function actionViewOrderDetails()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id']; 
    	$order_id= $this->data['order_id'];
    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	
		$_GET['backend']='true';
		if ( $data=Yii::app()->functions->getOrder2($order_id)){	
			//dump($data);					
			$json_details=!empty($data['json_details'])?json_decode($data['json_details'],true):false;
			if ( $json_details !=false){
			    Yii::app()->functions->displayOrderHTML(array(
			       'merchant_id'=>$data['merchant_id'],
			       'order_id'=>$order_id,
			       'delivery_type'=>$data['trans_type'],
			       'delivery_charge'=>$data['delivery_charge'],
			       'packaging'=>$data['packaging'],
			       'cart_tip_value'=>$data['cart_tip_value'],
				   'cart_tip_percentage'=>$data['cart_tip_percentage'],
				   'card_fee'=>$data['card_fee'],
				   'donot_apply_tax_delivery'=>$data['donot_apply_tax_delivery'],
				   'points_discount'=>isset($data['points_discount'])?$data['points_discount']:'' /*POINTS PROGRAM*/
			     ),$json_details,true);
			     $data2=Yii::app()->functions->details;
			     unset($data2['html']);			     
			     $this->code=1;
			     $this->msg="OK";
			     
			     $admin_decimal_separator=getOptionA('admin_decimal_separator');
		         $admin_decimal_place=getOptionA('admin_decimal_place');
		         $admin_currency_position=getOptionA('admin_currency_position');
		         $admin_thousand_separator=getOptionA('admin_thousand_separator');
			     
			     $data2['raw']['settings']=Driver::priceSettings();
			     $data2['raw']['order_info']=array(
			       'order_id'=>$data['order_id'],
			       'order_change'=>$data['order_change'],
			     );
			     
			     $this->details=$data2['raw'];			     
			     
			} else $this->msg = self::t("Record not found");
		} else $this->msg = self::t("Record not found");    	
    	$this->output();
    }
    
    public function actionGetNotifications()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id'];    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	
    	if ( $res=Driver::getDriverNotifications($driver_id)) {
    		 $data=array();
    		 foreach ($res as $val) {
    		 	$val['date_created']=Driver::prettyDate($val['date_created']);
    		 	//$val['date_created']=date("h:i:s",strtotime($val['date_created']));
    		 	$val['push_title']=Driver::t($val['push_title']);
    		 	$data[]=$val;
    		 }
    		 $this->code=1;
    		 $this->msg="OK";
    		 $this->details=$data;
    	} else $this->msg=self::t("No notifications");
    	$this->output();
    }
    
    public function actionUpdateDriverLocation()
    {    	
    	
    	//Driver::createLogs('test','track');
    	    	
    	$track_type = isset($this->data['track_type'])?$this->data['track_type']:'';
    	    	
    	if(empty($track_type)){
	    	$json = @file_get_contents('php://input');        
	        if(!empty($json)){                   
	            $json = json_decode($json,true);
	            if(!is_array($json) && count((array)$json)<=1){
	                Yii::app()->end();
	            }
	            $this->data = $json[0];
	        }        
    	}
        
        $this->data['token'] = isset($this->data['token'])?$this->data['token']:'';        
        
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id'];    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	
     
    	$params=array(
    	  'location_lat'=>$this->data['lat'],
    	  'location_lng'=>$this->data['lng'],
    	  'last_login'=>AdminFunctions::dateNow(),
	      'last_online'=>strtotime("now"),
	      'is_online'=>1,
	      'app_version'=>isset($this->data['app_version'])?$this->data['app_version']:'',
				'battery_level'=>isset($this->data['battery_level'])?$this->data['battery_level']:'-1',
				'battery_status'=>$this->data['battery_status'],
				'device_id'=>$this->data['device_id'],
        'activity_confidence'=>$this->data['activity_confidence'],
        'activity_type'=>$this->data['activity_type'],
        'speed'=>isset($this->data['speed'])?$this->data['speed']:'45',
        //'login_status'=>$this->data['login_status']
    	);
    	
    	if ( $token['on_duty']==2){
    	    unset($params['last_online']);
    	    unset($params['is_online']);
    	}   
    	
    	$db=new DbExt;
    	if ( $db->updateData("{{driver}}",$params,'driver_id',$driver_id)){
    		
    		$this->code=1;
    		$this->msg=self::t("Location set");
    		
    		/*log driver location*/    		
    		$is_record=getOption($token['customer_id'],'agents_record_track_Location');
    		if ($is_record==1){
	    		$logs=array(
	    		  'customer_id'=>$token['customer_id'],
	    		  'driver_id'=>$driver_id,
	    		  'latitude'=>$this->data['lat'],
	    	      'longitude'=>$this->data['lng'],
	    	      
	    	      'altitude'=>isset($this->data['altitude'])?$this->data['altitude']:'',
	    	      'accuracy'=>isset($this->data['accuracy'])?$this->data['accuracy']:'',
	    	      'altitudeAccuracy'=>isset($this->data['altitudeAccuracy'])?$this->data['altitudeAccuracy']:'',
	    	      'heading'=>isset($this->data['heading'])?$this->data['heading']:'',
	    	      'speed'=>isset($this->data['speed'])?$this->data['speed']:'45',
	    	      'track_type'=>isset($this->data['track_type'])?$this->data['track_type']:'',	    
	    	      	      
              'battery_level'=>isset($this->data['battery_level'])?$this->data['battery_level']:'-1',
              'battery_status'=>isset($this->data['battery_ischarging'])?$this->data['battery_ischarging']:'',
            
              'activity_confidence'=>isset($this->data['activity_confidence'])?$this->data['activity_confidence']:'-1',
              'activity_type'=>isset($this->data['activity_type'])?$this->data['activity_type']:'',
              'geofence_identifier'=>isset($this->data['geofence_identifier'])?$this->data['geofence_identifier']:'',
              'geofence_action'=>isset($this->data['geofence_action'])?$this->data['geofence_action']:'',
              'uuid'=>isset($this->data['uuid'])?$this->data['uuid']:'',
            
              'event'=>isset($this->data['event'])?$this->data['event']:'',
              'is_moving'=>isset($this->data['is_moving'])?$this->data['is_moving']:'-1',
            
              'odometer'=>isset($this->data['odometer'])?$this->data['odometer']:'-1',
              'timestamp'=>isset($this->data['timestamp'])?$this->data['timestamp']:AdminFunctions::dateNow(),
            
	    	      'date_created'=>AdminFunctions::dateNow(),
	    	      'ip_address'=>$_SERVER['REMOTE_ADDR'],	
	    	      'date_log'=>date("Y-m-d"),
						  'battery_level'=>isset($this->data['battery_level'])?$this->data['battery_level']:'-1',
							'battery_status'=>isset($this->data['battery_status'])?$this->data['battery_status']:'',	
              'activity_confidence'=>$this->data['activity_confidence'],
              'activity_type'=>$this->data['activity_type'],
              //'login_status'=>isset($this->data['login_status'])?$this->data['login_status']:0
	    		);
          
            $js_code = 'console.log driver update(' . json_encode($logs, JSON_HEX_TAG) . ');';
          error_log($js_code);
      
	    		$db->insertData("{{driver_track_location}}",$logs);
    	
	$app_track_interval=getOption($token['customer_id'],'app_track_interval');
					if (!is_numeric($app_track_interval)){
								$app_track_interval=8000;
					} else $app_track_interval=$app_track_interval*1000;

					if ($app_track_interval<=0){
								$app_track_interval=8000;
					}
          
          $this->details=array(
												'device_vibration'=>getOption($token['customer_id'],'driver_device_vibration'),
												'app_disabled_bg_tracking'=>getOption($token['customer_id'],'app_disabled_bg_tracking'),
												'app_track_interval'=>$app_track_interval,
											);
          
	    		$db->insertData("{{driver_track_location}}",$logs);
    		}  else {
              	$app_track_interval=getOption($token['customer_id'],'app_track_interval');
											if (!is_numeric($app_track_interval)){
												$app_track_interval=8000;
											} else $app_track_interval=$app_track_interval*1000;

											if ($app_track_interval<=0){
												$app_track_interval=8000;
											}
          
            		$this->details=array(
												'device_vibration'=>getOption($token['customer_id'],'driver_device_vibration'),
												'app_disabled_bg_tracking'=>getOption($token['customer_id'],'app_disabled_bg_tracking'),
												'app_track_interval'=>$app_track_interval,
											);
        }    		
    		    		    		
    	} else $this->msg="Failed";
    	$this->output();    	
    }
    
  
  function console_log($output, $with_script_tags = true) {
    $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . ');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
}
 
   /* public function actionlocations()
    {    	
      
      $this->data=$_REQUEST;
      
      $json = @file_get_contents('php://input');        
			  if(!empty($json)){                   
            $json = json_decode($json,true);
		        if(!is_array($json) && count((array)$json)<=1){
                Yii::app()->end();
            }
					 $js_code = 'console.log(' . json_encode($json['locations'], JSON_HEX_TAG) . ');';
			  	 error_log($js_code);
           $this->data = $json[0];
        }
      
      $js_code = 'console.log(' . json_encode($this->data['token'], JSON_HEX_TAG) . ');';
      error_log($js_code);
     
      	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id'];    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	
     
    	$params=array(
    	  'location_lat'=>$this->data['lat'],
    	  'location_lng'=>$this->data['lng'],
    	  'last_login'=>AdminFunctions::dateNow(),
	      'last_online'=>strtotime("now"),
	      'is_online'=>1,
	      'app_version'=>isset($this->data['app_version'])?$this->data['app_version']:'',
				'battery_level'=>isset($this->data['battery_level'])?$this->data['battery_level']:'-1',
				'battery_status'=>$this->data['battery_status'],
				'device_id'=>$this->data['device_id'],
        'activity_confidence'=>$this->data['activity_confidence'],
        'activity_type'=>$this->data['activity_type'],
        
        //'login_status'=>$this->data['login_status']
    	);
    	
    	if ( $token['on_duty']==2){
    	    unset($params['last_online']);
    	    unset($params['is_online']);
    	}   
    	
    	$db=new DbExt;
    	if ( $db->updateData("{{driver}}",$params,'driver_id',$driver_id)){
    		
    		$this->code=1;
    		$this->msg=self::t("Location set");
        
    		
    		/*log driver location    		
    		$is_record=getOption($token['customer_id'],'agents_record_track_Location');
    		if ($is_record==1){
	    		$logs=array(
	    		  'customer_id'=>(integer)$token['customer_id'],
	    		  'driver_id'=>(integer)$driver_id,
	    		  'latitude'=>$this->data['lat'],
	    	      'longitude'=>$this->data['lng'],
	    	      
	    	      'altitude'=>isset($this->data['altitude'])?$this->data['altitude']:'',
	    	      'accuracy'=>isset($this->data['accuracy'])?$this->data['accuracy']:'',
	    	      'altitudeAccuracy'=>isset($this->data['altitudeAccuracy'])?$this->data['altitudeAccuracy']:'',
	    	      'heading'=>isset($this->data['heading'])?$this->data['heading']:'',
	    	      'speed'=>isset($this->data['speed'])?$this->data['speed']:'',
	    	      'track_type'=>isset($this->data['track_type'])?$this->data['track_type']:'',	    
	    	  
              'activity_confidence'=>isset($this->data['activity_confidence'])?$this->data['activity_confidence']:'-1',
              'activity_type'=>isset($this->data['activity_type'])?$this->data['activity_type']:'',
              'geofence_identifier'=>isset($this->data['geofence_identifier'])?$this->data['geofence_identifier']:'',
              'geofence_action'=>isset($this->data['geofence_action'])?$this->data['geofence_action']:'',
              'uuid'=>isset($this->data['uuid'])?$this->data['uuid']:'',
            
              'event'=>isset($this->data['event'])?$this->data['event']:'',
              'is_moving'=>isset($this->data['is_moving'])?$this->data['is_moving']:'-1',
            
              'odometer'=>isset($this->data['odometer'])?$this->data['odometer']:'-1',
              'timestamp'=>isset($this->data['timestamp'])?$this->data['timestamp']:AdminFunctions::dateNow(),
            
	    	      'date_created'=>AdminFunctions::dateNow(),
	    	      'ip_address'=>$_SERVER['REMOTE_ADDR'],	
	    	      'date_log'=>date("Y-m-d"),
				  		'battery_level'=>isset($this->data['battery_level'])?$this->data['battery_level']:'-1',
							'battery_status'=>isset($this->data['battery_status'])?$this->data['battery_status']:'-1',	
              'activity_confidence'=>$this->data['activity_confidence'],
              'activity_type'=>$this->data['activity_type'],
              //'login_status'=>isset($this->data['login_status'])?$this->data['login_status']:0
	    		);
          
            $js_code = 'console.log(' . json_encode($logs, JSON_HEX_TAG) . ');';
          error_log($js_code);
      
	    		$db->insertData("{{driver_track_location}}",$logs);
    	
	$app_track_interval=getOption($token['customer_id'],'app_track_interval');
					if (!is_numeric($app_track_interval)){
								$app_track_interval=8000;
					} else $app_track_interval=$app_track_interval*1000;

					if ($app_track_interval<=0){
								$app_track_interval=8000;
					}
          
          $this->details=array(
												'device_vibration'=>getOption($token['customer_id'],'driver_device_vibration'),
												'app_disabled_bg_tracking'=>getOption($token['customer_id'],'app_disabled_bg_tracking'),
												'app_track_interval'=>$app_track_interval,
											);
          
	    		$db->insertData("{{driver_track_location}}",$logs);
    		}  else {
              	$app_track_interval=getOption($token['customer_id'],'app_track_interval');
											if (!is_numeric($app_track_interval)){
												$app_track_interval=8000;
											} else $app_track_interval=$app_track_interval*1000;

											if ($app_track_interval<=0){
												$app_track_interval=8000;
											}
          
            		$this->details=array(
												'device_vibration'=>getOption($token['customer_id'],'driver_device_vibration'),
												'app_disabled_bg_tracking'=>getOption($token['customer_id'],'app_disabled_bg_tracking'),
												'app_track_interval'=>$app_track_interval,
											);
        }    		
    		    		    		
    	} else $this->msg="Failed";
    	$this->output();    	
	}*/
	
	// public function actionlocations()
    // {    	
	
    //   $this->data=$_REQUEST;
      
    //   $json = @file_get_contents('php://input');        
    //     if(!empty($json)){                   
    //         $json = json_decode($json,true);
    //         if(!is_array($json) && count((array)$json)<=1){
    //             Yii::app()->end();
    //         }
    //        $posts=$json['location'];
    //     }
		 
	// 	foreach($posts as $key => $value) {

	// 	}
		
		 
    // 	$this->output();    	
    // }
  
    public function actionlocations()
    {    	
	
      $this->data=$_REQUEST;
      
      $json = @file_get_contents('php://input');        
        if(!empty($json)){                   
            $json = json_decode($json,true);
            if(!is_array($json) && count((array)$json)<=1){
                Yii::app()->end();
            }
           $posts=$json['location'];// $this->data = $json[0];
        }
		 
		// $js_code = 'actionlocations -> console.1(' . json_encode($posts, JSON_HEX_TAG) . ');';
		// error_log($js_code);

		$pExtrasTemp=array();
		 foreach($posts as $obj){
			 


			if (empty($obj['extras'])) {
				
				if(count((array)$pExtrasTemp)==0 ){
					Yii::app()->end();
				}
				
				$obj['extras']= (array)$pExtrasTemp;
				// $js_code = 'actionlocations -> temosmosmos extras (' . json_encode($obj['extras'], JSON_HEX_TAG) . ');';
				// error_log($js_code);
				// $this->msg=self::t("Object extras Null");
				// 	$this->output();
				// 	Yii::app()->end();
				//    return;
			} else {
				// $js_code = 'actionlocations -> not extras (' . json_encode($obj['extras'], JSON_HEX_TAG) . ');';
				// error_log($js_code);
				$pExtrasTemp = array(
					'token'=>$obj['extras']['token'],
					  'user_id'=>$obj['extras']['user_id'],
					  'api_key'=>$obj['extras']['api_key'],
					  'track_type'=>$obj['extras']['track_type'],
					  'device_id'=>$obj['extras']['device_id']
				);
			}
	  
		
	  if (!empty($obj['extras']['token'])) $sToken = $obj['extras']['token'];
	

	  if (strlen($sToken) == 0 || empty($sToken)) {
		$this->msg=self::t("Token Null");
		$this->output();
		Yii::app()->end();
			 
		   return; }
	  
	  if (!empty($obj['extras']['device_id'])) $deviceID =  $obj['extras']['device_id'];
			 
	  if ( !$token=Driver::getDriverByToken($sToken) ) {
			$this->msg=self::t("Token not valid or Device is empty");
    		$this->output();
    		Yii::app()->end();
							 
		}     
		
		if (strlen($deviceID) == 0) {
			$this->msg=self::t("deviceID Null");
			$this->output();
			Yii::app()->end();
				 
			   return; }
		
		// $js_code = 'actionlocations -> console.3(' . json_encode($token, JSON_HEX_TAG) . ');';
		// error_log($js_code);

    	$driver_id=$token['driver_id'];    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
			 
	    
      $icharging=0;
      if ($obj['battery']['is_charging'] == true) $icharging=1;
      
      $ismoving=0;
      if ($obj['is_moving'] == true) $ismoving=1;

			if (!empty($obj['battery']['level'])) {
    		$batterylevel = $obj['battery']['level'] * 100;
			}
			 
    	$params=array(
    	  'location_lat'=>$obj['coords']['latitude'],
    	  'location_lng'=>$obj['coords']['longitude'],
        'battery_level'=>$batterylevel, //isset($obj['battery']['level'])?$obj['battery']['level']:'-1',
        'battery_status'=>$icharging,
        'activity_confidence'=>isset($obj['activity']['confidence'])?$obj['activity']['confidence']:'-1',
        'activity_type'=>isset($obj['activity']['type'])?$obj['activity']['type']:'',
    	  'last_login'=>AdminFunctions::dateNow(),
	      'last_online'=>strtotime("now"),
		  'is_online'=>1,
		  'speed'=>isset($obj['coords']['speed'])?$obj['coords']['speed']:'45',
	      'app_version'=>isset($obj['extras']['app_version'])?$obj['extras']['app_version']:'1.5',
				'device_id'=>isset($obj['extras']['device_id'])?$obj['extras']['device_id']:'device_123456789'
    	);
    	
    	if ( $token['on_duty']==2){
    	    unset($params['last_online']);
    	    unset($params['is_online']);
    	}   
		
		// $js_code = 'actionlocations ->  console.5 ---> update driver; ' . $driver_id;
		//   error_log($js_code);
		  
		
		//   $js_code = 'actionlocations -> console.5.1(' . json_encode($params, JSON_HEX_TAG) . ');';
		//   error_log($js_code);

		  $up =Yii::app()->db->createCommand()->update("{{driver}}",$params,
  	    'driver_id=:driver_id',
	  	    array(
	  	      ':driver_id'=>$driver_id
	  	    )
		  );  	 
		  
    	// $db=new DbExt;
    	// if ( $db->updateData("{{driver}}",$params,'driver_id',$driver_id)){
		if ($up){
    // 					  $js_code = 'actionlocations ->  console.6 ---> update driver;';
	//   error_log($js_code);


    		$this->code=1;
    		$this->msg=self::t("Location set");
    		
    		/*log driver location*/    		
    		$is_record=getOption($token['customer_id'],'agents_record_track_Location');
    		if ($is_record==1){
          
//           $vtimestamp=substr(str_replace('T','-',$obj['timestamp']));
	    		$logs=array(
	    		  'customer_id'=>$token['customer_id'],
	    		  'driver_id'=>$driver_id,
	    		  'latitude'=>$obj['coords']['latitude'],
	    	    'longitude'=>$obj['coords']['longitude'],
	    	      'altitude'=>isset($obj['coords']['altitude'])?$obj['coords']['altitude']:'',
	    	      'accuracy'=>isset($obj['coords']['accuracy'])?$obj['coords']['accuracy']:'',
	    	      'altitudeAccuracy'=>isset($obj['coords']['altitude_accuracy'])?$obj['coords']['altitude_accuracy']:'',
	    	      'heading'=>isset($obj['coords']['heading'])?$obj['coords']['heading']:'',
	    	      'speed'=>isset($obj['coords']['speed'])?$obj['coords']['speed']:'',
	    	      'track_type'=>isset($obj['extras']['track_type'])?$obj['extras']['track_type']:'', 
              'battery_level'=>$batterylevel,//isset($obj['battery']['level'])?$obj['battery']['level']:'-1',
              'battery_status'=>$icharging,
              'activity_confidence'=>isset($obj['activity']['confidence'])?$obj['activity']['confidence']:'-1',
              'activity_type'=>isset($obj['activity']['type'])?$obj['activity']['type']:'',
              'geofence_identifier'=>isset($obj['geofence']['identifier'])?$obj['geofence']['identifier']:'',
              'geofence_action'=>isset($obj['geofence']['action'])?$obj['geofence']['action']:'',
              'uuid'=>isset($obj['uuid'])?$obj['uuid']:'',
              'event'=>isset($obj['event'])?$obj['event']:'',
              'is_moving'=>$ismoving,
              'odometer'=>isset($obj['odometer'])?$obj['odometer']:'-1',
              'timestamp'=>isset($obj['timestamp'])?$obj['timestamp']:AdminFunctions::dateNow(),
              'date_created'=>AdminFunctions::dateNow(),
	    	      'ip_address'=>$_SERVER['REMOTE_ADDR'],	
	    	      'date_log'=>date("Y-m-d")
	    		);
// 					  $js_code = 'console.log(' . json_encode($logs, JSON_HEX_TAG) . ');';
//         	error_log($js_code);
					
				// $db->insertData("{{driver_track_location}}",$logs);
				Yii::app()->db->createCommand()->insert("{{driver_track_location}}",$logs);

				// 	  $js_code = 'console.4(' . json_encode($logs, JSON_HEX_TAG) . ');';
				//   error_log($js_code);
    		}    		
    		    		    		
		} else {
			$this->msg="Failed";
			// $js_code = 'actionlocations ->  console.7 ---> update driver;';
			// error_log($js_code);
			}
		 }
		 
    	$this->output();    	
    }
  
    public function actionClearNofications()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id'];
    	$stmt="UPDATE 
    	{{driver_pushlog}}
    	SET
    	is_read='1'
    	WHERE
    	driver_id=".self::q($driver_id)."
    	AND
    	is_read='2'
    	";
    	$this->code=1;
    	$this->msg="OK";
    	$db=new DbExt;
    	$db->qry($stmt);
    	$this->output();    	
    }
    
    public function actionLogout()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	
    	$driver_id=$token['driver_id'];
    	
    	$tracking_type=Driver::getTrackingOptions( $token['customer_id'] );
    	if ( $tracking_type==2){
    	    $last_online=strtotime("-31 minutes");
    	    $last_login = date("Y-m-d H:i:s", strtotime("-31 minutes"));
    	} else {
    		$last_online=strtotime("-6 minutes");
    		$last_login = date("Y-m-d H:i:s", strtotime("-6 minutes"));
    	}
    	    	    
    	$params=array(    	  
    	  'last_online'=>$last_online,
    	  'on_duty'=>2,
    	  'ip_address'=>$_SERVER['REMOTE_ADDR'],
    	  'is_online'=>2,
    	  'last_login'=>$last_login,
    	  'enabled_push'=>2,
    	  'device_id'=>''
    	);
    	
    	$db=new DbExt;
    	$db->updateData('{{driver}}',$params,'driver_id',$driver_id);
    	$this->code=1;
    	$this->msg="OK";
    	unset($db);
    	$this->output();
    }
    
		public function actionLoadNotesM()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	
    	$driver_id=$token['driver_id'];  
    	Driver::setCustomerTimezone( $token['customer_id'] );  	
    	    	
    	if ( $res=Driver::getTaskId($this->data['task_id']) ){    		
    		if ( $notes=Driver::getNotes($res['task_id'])){
    			$data=array();
					$datatmp=array();
    			foreach ($notes as $val) {
						$datatmp['id']=$val['id'];
						$datatmp['notes']=$val['notes'];
    				$datatmp['status_raw']=$val['status'];
    				$datatmp['status']=self::t($val['status']);
    				$datatmp['date_created']=Driver::prettyDate($val['date_created']);
						$data[]=$datatmp;
    			}
    			$this->code=1;
    			$this->msg=$res['status'];
    			$this->details=$data;
    		} else $this->msg=self::t("no results");
    	} else $this->msg=self::t("Task not found");
    	
    	$this->output();
    }
	
    public function actionLoadNotes()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	
    	$driver_id=$token['driver_id'];  
    	Driver::setCustomerTimezone( $token['customer_id'] );  	
    	    	
    	if ( $res=Driver::getTaskId($this->data['task_id']) ){    		
    		if ( $notes=Driver::getNotes($res['task_id'])){
    			$data=array();
    			foreach ($notes as $val) {
    				$val['status_raw']=$val['status'];
    				$val['status']=self::t($val['status']);
    				$val['date_created']=Driver::prettyDate($val['date_created']);
    				$data[]=$val;
    			}
    			$this->code=1;
    			$this->msg=$res['status'];
    			$this->details=$data;
    		} else $this->msg=self::t("no results");
    	} else $this->msg=self::t("Task not found");
    	
    	$this->output();
    }
    
    public function actionAddNotes()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	
    	$driver_id=$token['driver_id'];    	
    	$driver_name=$token['first_name'] ." " .$token['last_name'];    
    	
    	Driver::setCustomerTimezone( $token['customer_id'] );	
    	
    	if ( $res=Driver::getTaskId($this->data['task_id']) ){    		
	    	if (isset($this->data['notes'])){
	    	 	if(!empty($this->data['notes'])){
	    	 		$db=new DbExt;
	    	 		$params=array(
	    	 		   'status'=>"notes",
	    	 		   'remarks'=>Driver::driverStatusPretty( $driver_name ,'notes'),
	    	 		   'task_id'=>$this->data['task_id'],
	    	 		   'driver_id'=>$driver_id,
	    	 		   'driver_location_lat'=>isset($token['location_lat'])?$token['location_lat']:'',
		               'driver_location_lng'=>isset($token['location_lng'])?$token['location_lng']:'',
		               'date_created'=>AdminFunctions::dateNow(),
		               'ip_address'=>$_SERVER['REMOTE_ADDR'],
		               'reason'=>"",
		               'notes'=>$this->data['notes']
	    	 		);
	    	 		if ( $db->insertData("{{task_history}}",$params)){
	    	 			$this->code=1; $this->msg="OK";
	    	 			$this->details=array(
	    	 			  'task_id'=>$this->data['task_id'],
	    	 			  'driver_id'=>$driver_id
	    	 			);
	    	 			
	    	 			$task_info=$res;
	    	 			$task_info['notes']=$this->data['notes'];
	    	 			
	    	 			if ( $task_info['trans_type']=="delivery"){ 
	    	 				Driver::sendCustomerNotification('DELIVERY_NOTES',$task_info);
	    	 			} else Driver::sendCustomerNotification('PICKUP_NOTES',$task_info);
	    	 			
	    	 		} else $this->msg=self::t("cannot saved notes");
	    	 		
	    	 		unset($db);
	    	 	} else $this->msg=self::t("Notes is required");
	    	} else $this->msg=self::t("Notes is required");
    	} else $this->msg=self::t("Task not found");
    	$this->output();
    }
    
    public function actionDeleteNotes()
    {
    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	
    	$driver_id=$token['driver_id'];    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	    	
    	if ( $res=Driver::getTaskId($this->data['task_id']) ){       		
    		Driver::deleteNotes($this->data['id']);
    		$this->msg="OK";
    		$this->code=1;
    		$this->details=array(
 			  'task_id'=>$this->data['task_id'],
 			  'driver_id'=>$driver_id
 			);
    	} else $this->msg=self::t("Task not found");
    	
    	$this->output();
    }
    
    public function actionUpdateNotes()
    {
    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	
    	$driver_id=$token['driver_id']; 
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	    	
    	if ( $res=Driver::getTaskId($this->data['task_id']) ){    		
    		if(isset($this->data['id'])){
    			$db=new DbExt;
    	 		$params=array(    	 		   
    	 		   'notes'=>$this->data['notes'],    	 		       	 		   
    	 		   'driver_location_lat'=>isset($token['location_lat'])?$token['location_lat']:'',
	               'driver_location_lng'=>isset($token['location_lng'])?$token['location_lng']:'',
	               'date_created'=>AdminFunctions::dateNow(),
	               'ip_address'=>$_SERVER['REMOTE_ADDR'],
	               'reason'=>""
    	 		);    	 		
    	 		if ( $db->updateData("{{task_history}}",$params,'id',$this->data['id'])){
    	 		 	$this->code=1;
    	 		 	$this->msg=self::t("Notes updated");
    	 		 	$this->details=array(
		 			  'task_id'=>$this->data['task_id'],
		 			  'driver_id'=>$driver_id
		 			);
		 			
		 			$task_info=$res;
    	 			$task_info['notes']=$this->data['notes'];
    	 				    	 			
    	 			if ( $task_info['trans_type']=="delivery"){ 
    	 				Driver::sendCustomerNotification('DELIVERY_UPDATE_NOTES',$task_info);
    	 			} else Driver::sendCustomerNotification('PICKUP_UPDATE_NOTES',$task_info);
		 			
    	 		} else $this->msg=self::t("cannot saved notes");
    		} else $this->msg=self::t("ID is required");
    	} else $this->msg=self::t("Task not found");
    	    	
    	$this->output();
    }
    
    public function actionUploadTaskPhoto()
    {
    	
    	$this->data=$_REQUEST;
    	$request=json_encode($_REQUEST);
    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		echo "$this->code|$this->msg|$this->details|".$request;
    		Yii::app()->end();
    	} 
    	
    	$driver_id=$token['driver_id'];  
    	$driver_name=$token['first_name'] ." " .$token['last_name'];  
    	
    	Driver::setCustomerTimezone( $token['customer_id'] );  	
    	  	
    	if ( $res=Driver::getTaskId($this->data['task_id']) ){    		
    	
    		 $task_id=$res['task_id'];
    		
    		 $path_to_upload=Driver::driverUploadPath();
    		
    		 if(isset($_FILES['file'])){
    		 	
    		 	header('Access-Control-Allow-Origin: *');
    		 	
    		 	$new_image_name = urldecode($_FILES["file"]["name"]).".jpg";	
		        $new_image_name=str_replace(array('?',':'),'',$new_image_name);
		        
		        if(@move_uploaded_file($_FILES["file"]["tmp_name"], "$path_to_upload/".$new_image_name)){		        
			        $params=array(
			           'status'=>"photo",
			           'remarks'=>Driver::driverStatusPretty($driver_name,"photo"),
			           'task_id'=>$task_id,
			           'driver_id'=>$driver_id,
			           'driver_location_lat'=>isset($token['location_lat'])?$token['location_lat']:'',
			           'driver_location_lng'=>isset($token['location_lng'])?$token['location_lng']:'',
			           'reason'=>'',
			           'date_created'=>AdminFunctions::dateNow(),
			           'ip_address'=>$_SERVER['REMOTE_ADDR'],	
			           'photo_name'=>$new_image_name
			        );
			        
			        $db=new DbExt;
			        if($db->insertData("{{task_history}}",$params)){
			           $this->code=1;
				       $this->msg=self::t("Upload successful");
	    		       $this->details=$task_id;
	    		       
	    		       $photo_link=websiteUrl()."/upload/photo/".$new_image_name;
	    		       
	    		       $task_info=$res;
    	 			   $task_info['photo_link']=$photo_link;
    	 				    	 			
    	 			   if ( $task_info['trans_type']=="delivery"){ 
    	 				   Driver::sendCustomerNotification('DELIVERY_PHOTO',$task_info);
    	 			   } else Driver::sendCustomerNotification('PICKUP_PHOTO',$task_info);	    		       
	    		       
			        } else $this->msg=self::t("failed cannot insert record");
		        } else $this->msg=self::t("Cannot upload photo");
		        		            		    
    		 } else $this->msg=self::t("Image is missing");
    		 
    	} else $this->msg=self::t("Task not found");	
    	
    	echo "$this->code|$this->msg|$this->details|".$request;
    }
    
    public function actionGetTaskPhoto()
    {
    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	
    	$driver_id=$token['driver_id'];    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	    	
    	if ( $res=Driver::getTaskId($this->data['task_id']) ){    		    		
    		if ( $photos=Driver::getTaskPhoto($this->data['task_id'])){
	    		$this->code=1;
	    		$this->msg=$res['status'];
	    		$this->details=$photos;
	    	} else $this->msg=self::t("No photo to show");    		
    		
    	} else $this->msg=self::t("Task not found");
    	    	
    	$this->output();
    }
	
		public function actionGetTaskPhotoM()
    {
    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	
    	$driver_id=$token['driver_id'];    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	    	
    	if ( $res=Driver::getTaskId($this->data['task_id']) ){    		    		
    		if ( $photos=Driver::getTaskPhoto($this->data['task_id'])){
					$data=array();
					$datatmp=array();
    			foreach ($photos as $val) {
						$datatmp['id']=$val['id'];
    				$datatmp['status']=self::t($val['status']);
						$datatmp['photo_name']=$val['photo_name'];
						$datatmp['photo_url']=$val['photo_url'];
    				$datatmp['date_created']=Driver::prettyDate($val['date_created']);
						$data[]=$datatmp;
    			}
	    		$this->code=1;
	    		$this->msg=$res['status'];
	    		$this->details=$data;
	    	} else $this->msg=self::t("No photo to show");    		
    		
    	} else $this->msg=self::t("Task not found");
    	    	
    	$this->output();
    }
    
    public function actionLoadSignature()
    {    
    
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	
    	$driver_id=$token['driver_id'];    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	    	
    	if ( $res=Driver::getTaskId($this->data['task_id']) ){    		    		
    		$task_id=$res['task_id'];
    		if ( $data=Driver::getLastSignature($task_id)){    			
    			$this->msg="OK";
    			$this->code=1;
    			if (!empty($data['customer_signature'])){
    				$data['customer_signature_url']=Driver::uploadURL()."/".$data['customer_signature'];
    				if (!file_exists(Driver::uploadPath()."/".$data['customer_signature'])){
    					$data['customer_signature_url']='';
    				}
    			}
    			
    			$this->details=array(
    			  'task_id'=>$task_id,
    			  'status'=>$res['status'],
    			  'data'=>$data
    			);
    		} else $this->msg=self::t("no signature found");
    	} else $this->msg=self::t("Task not found");
    	$this->output();
    }
	
	 public function actionLoadSignatureM()
    {    
    
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	
    	$driver_id=$token['driver_id'];    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	    	
    	if ( $res=Driver::getTaskId($this->data['task_id']) ){    		    		
    		$task_id=$res['task_id'];
    		if ( $data=Driver::getLastSignature($task_id)){    			
    			$this->msg="OK";
    			$this->code=1;
    			if (!empty($data['customer_signature'])){
						if (file_exists(Driver::driverUploadPathSig()."/".$data['customer_signature'])){
														$data['customer_signature_url']=Driver::uploadURL()."/signature/".$data['customer_signature'];
													}
//     				$data['customer_signature_url']=Driver::uploadURL()."/".$data['customer_signature'];
//     				if (!file_exists(Driver::uploadPath()."/".$data['customer_signature'])){
//     					$data['customer_signature_url']='';
//     				}
    			}
					
						$datatmp=array();
    				$datatmp['id']=$data['id'];
    				$datatmp['status']=self::t($data['status']);
						$datatmp['customer_signature']=$data['customer_signature'];
						$datatmp['customer_signature_url']=$data['customer_signature_url'];
    				$datatmp['date_created']=Driver::prettyDate($data['date_created']);
						
    			$this->details=array(
    			  'task_id'=>$task_id,
    			  'status'=>$res['status'],
    			  'data'=>$datatmp
    			);
    		} else $this->msg=self::t("no signature found");
    	} else $this->msg=self::t("Task not found");
    	$this->output();
    }

    public function actionUploadProfile()
    {
    	$this->data=$_REQUEST;
    	
    	$request=json_encode($_REQUEST);
    	
    	if (!isset($this->data['token'])){
    		$this->data['token']='';
    	}
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("token not found");
    		echo "$this->code|$this->msg||".$request;
    		Yii::app()->end();
    	}     	    	
    	
    	$driver_id=$token['driver_id'];  
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	
    	$path_to_upload=Driver::driverUploadPath();
    	if(!file_exists($path_to_upload)) {	
           if (!@mkdir($path_to_upload,0777)){           	               	
           	    $this->msg=Driver::t("Error has occured cannot create upload directory");
                $this->jsonResponse();
           }		    
	    }
	    
	    $profile_photo='';
	    	    	    
	    if(isset($_FILES['file'])){
	    	
	    	header('Access-Control-Allow-Origin: *');
	    	
		    $new_image_name = urldecode($_FILES["file"]["name"]).".jpg";	
		    $new_image_name = str_replace(array('?',':'),'',$new_image_name);
		        
		    @move_uploaded_file($_FILES["file"]["tmp_name"], "$path_to_upload/".$new_image_name);
		    
		    $db=new DbExt;
		    $params=array(
		     'profile_photo'=>$new_image_name,
		     'date_modified'=>AdminFunctions::dateNow()
		    );
		    if($db->updateData("{{driver}}",$params,'driver_id',$driver_id)){
			    $this->code=1;
			    $this->msg=t("Upload successful");
			    $this->details=$new_image_name;
			    $profile_photo=websiteUrl()."/upload/photo/".$new_image_name;
		    } else $this->msg=self::t("Error cannot update");
		    
	    } else $this->msg=self::t("Image is missing");
	    
    	echo "$this->code|$this->msg|$profile_photo|".$request;
    }
    
    public function actionDeletePhoto()
    {
    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	
    	$driver_id=$token['driver_id'];    	
    	
    	if (isset($this->data['id'])){
    		if ( $res=Driver::getTaskId($this->data['task_id']) ){  
    			if ( $data=Driver::getTasHistoryByID($this->data['id'])){    				
    				$file=Driver::driverUploadPath()."/".$data['photo_name'];
    				if (file_exists($file)){
    					@unlink($file);
    				}
		    		Driver::deleteSignature($this->data['id']);
		    		$this->code=1; $this->msg="OK";
		    		$this->details=$this->data['task_id'];
    			} else $this->msg=self::t("Task not found");
    		} else $this->msg=self::t("Task not found");
    	} else $this->msg=self::t("missing parameters");
    	    	
    	$this->output();
    }

	public function actionLoadContact()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	
    	$driver_id=$token['driver_id'];  
    	Driver::setCustomerTimezone( $token['customer_id'] );  	
    	    	
    	if ( $res=Driver::getGeoContactList($token['customer_id']) ){    		
    			$data='';
    			foreach ($res as $val) {
					$val['contact_id']=$val['contact_id'];
					$val['customer_id']=$val['customer_id'];
					$val['fullname']=$val['fullname'];
					$val['email']=$val['email'];
					$val['phone']=$val['phone'];
					$val['address']=$val['address'];
					$val['addresss_lat']=$val['addresss_lat'];
					$val['addresss_lng']=$val['addresss_lng'];
					$val['date_created']=Driver::prettyDate($val['date_created']);
					$val['date_modified']=$val['date_modified'];
					$val['ip_address']=$val['ip_address'];
					$val['router_id']=$val['router_id'];
					$val['finder_id']=$val['finder_id'];
					$val['cat_id']=$val['cat_id'];
					$val['driver_id']=$val['driver_id'];
    				$val['status']=self::t($val['status']);
					$val['photo_name']=$val['photo_name'];
					$val['photo_url']=$val['photo_url'];
    				$data[]=$val;
    			}
    			$this->code=1;
    			$this->msg="OK";
    			$this->details=$data;
    	} else $this->msg=self::t("Contact not found");
    	
    	$this->output();
    }
    
     public function actionAddContact()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	
    	$driver_id=$token['driver_id'];    	
    	$driver_name=$token['first_name'] ." " .$token['last_name'];    
    	Driver::setCustomerTimezone( $token['customer_id'] );	
		 			$params=array(
			           'customer_id'=>$token['customer_id'],
			           'fullname'=>$this->data['fullname'],
					   'email'=>$this->data['email'],
					   'phone'=>$this->data['phone'],
					   'address'=>$this->data['address'],
					   'addresss_lat'=>$this->data['addresss_lat'],
					   'addresss_lng'=>$this->data['addresss_lng'],
					   'date_created'=>AdminFunctions::dateNow(),
					   'ip_address'=>$_SERVER['REMOTE_ADDR'],
					   'router_id'=>$this->data['router_id'],
					   'cat_id'=>$this->data['cat_id'],
					   'driver_id'=>$driver_id,
					   'status'=>$this->data['status']
			        //    'driver_location_lat'=>isset($token['location_lat'])?$token['location_lat']:'',
			        //    'driver_location_lng'=>isset($token['location_lng'])?$token['location_lng']:'',
			        //    'reason'=>'',
			        //    'date_created'=>AdminFunctions::dateNow(),
			        //    'ip_address'=>$_SERVER['REMOTE_ADDR'],	
			        //    'photo_name'=>$new_image_name
			        );
			        $db=new DbExt;
			        if($db->insertData("{{contacts}}",$params)){
					   $contact_id=Yii::app()->db->getLastInsertID();
			           $this->code=1;
				       $this->msg=self::t("update successful");
	    		       $this->details=$contact_id;
	    		       
	    		       //$photo_link=websiteUrl()."/upload/photo/".$new_image_name;
	    		       
	    		       //$task_info=$res;
    	 			   //$task_info['photo_link']=$photo_link;
    	 				
			        } else $this->msg=self::t("failed cannot insert record");
    	$this->output();
    }
    
    
    public function actionDeleteContact()
    {
    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	
    	$driver_id=$token['driver_id'];    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
		if ( $res=Driver::getGeoContactByID($this->data['id'],$token['customer_id']) ){       		
			Driver::deleteGeoContact($this->data['id']);
			$this->msg="OK";
    		$this->code=1;
    		$this->details=array(
 			  'contact_id'=>$this->data['id'],
 			  'driver_id'=>$driver_id
 			);
    	} else $this->msg=self::t("Contact not found");
    	
    	$this->output();
    }
    
    public function actiongetTaskCompleted()
    {
    	$this->actionGetTaskByDate();
    }
    
     public function actionUpdateContact()
    {
    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	
    	$driver_id=$token['driver_id']; 
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	
    	if ( $res=Driver::getGeoContactByID($this->data['id'],$token['customer_id']) ){    		
    		if(isset($this->data['id'])){
    			$db=new DbExt;
    	 		$params=array(  
				   	   'customer_id'=>$token['customer_id'],
			           'fullname'=>$this->data['fullname'],
					   'email'=>$this->data['email'],
					   'phone'=>$this->data['phone'],
					   'address'=>$this->data['address'],
					   'addresss_lat'=>$this->data['addresss_lat'],
					   'addresss_lng'=>$this->data['addresss_lng'],
					   'date_modified'=>AdminFunctions::dateNow(),
					   'ip_address'=>$_SERVER['REMOTE_ADDR'],
					   'router_id'=>$this->data['router_id'],
					   'cat_id'=>$this->data['cat_id'],
					   'driver_id'=>$driver_id,
					   'status'=>$this->data['status'] 		   
    	 		);    	 		
    	 		if ( $db->updateData("{{contacts}}",$params,'contact_id',$this->data['id'])){
    	 		 	$this->code=1;
    	 		 	$this->msg=self::t("Contacts updated");
    	 		 	$this->details=array(
		 			  'contact_id'=>$this->data['id'],
		 			  'driver_id'=>$driver_id
		 			);
    	 		} else $this->msg=self::t("cannot saved notes");
    		} else $this->msg=self::t("ID is required");
    	} else $this->msg=self::t("Contact not found");
    	    	
    	$this->output();
    }
    
	public function actionUploadContactPhoto()
    {
    	
    	$this->data=$_REQUEST;
    	$request=json_encode($_REQUEST);
    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		echo "$this->code|$this->msg|$this->details|".$request;
    		Yii::app()->end();
    	} 
    	
    	$driver_id=$token['driver_id'];  
    	$driver_name=$token['first_name'] ." " .$token['last_name'];  
    	
    	Driver::setCustomerTimezone( $token['customer_id'] );  	
    	if ( $res=Driver::getGeoContactByID($this->data['id'], $token['customer_id']) ){    		
    	
    		 $contact_id=$res['id'];
    		
    		 $path_to_upload=Driver::driverUploadGeoPath();
			 if(isset($_FILES['file'])){
    		 	
    		 	header('Access-Control-Allow-Origin: *');
    		 	
    		 	$new_image_name = urldecode($_FILES["file"]["name"]).".jpg";	
		        $new_image_name=str_replace(array('?',':'),'',$new_image_name);
		        
		        if(@move_uploaded_file($_FILES["file"]["tmp_name"], "$path_to_upload/".$new_image_name)){		        
			        $params=array(
					   'driver_id'=>$driver_id,
			           'date_created'=>AdminFunctions::dateNow(),
			           'ip_address'=>$_SERVER['REMOTE_ADDR'],	
			           'photo_name'=>$new_image_name
			        );
			        
			        $db=new DbExt;
			        if($db->updateData("{{contacts}}",$params,'contact_id',$this->data['id'])){
			           $this->code=1;
				       $this->msg=self::t("Upload successful");
	    		       $this->details=$contact_id;
	    		       
	    		       $photo_link=websiteUrl()."/upload/contacts/".$new_image_name;
	    		       
	    		    //    $task_info=$res;
    	 			//    $task_info['photo_link']=$photo_link;
    	 				    	 			
    	 			//    if ( $task_info['trans_type']=="delivery"){ 
    	 			// 	   Driver::sendCustomerNotification('DELIVERY_PHOTO',$task_info);
    	 			//    } else Driver::sendCustomerNotification('PICKUP_PHOTO',$task_info);	    		       
	    		       
			        } else $this->msg=self::t("failed cannot insert record");
		        } else $this->msg=self::t("Cannot upload photo");
		        		            		    
    		 } else $this->msg=self::t("Image is missing");
    		 
    	} else $this->msg=self::t("contact not found");	
    	
    	echo "$this->code|$this->msg|$this->details|".$request;
    }
    
    public function actionGetContactPhoto()
    {
    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	
    	$driver_id=$token['driver_id'];    	
    	Driver::setCustomerTimezone( $token['customer_id'] );
    	    	
    	if ( $res=Driver::getGeoContactByID($this->data['id']) ){    		    		
    		if ( $photos=Driver::getTaskPhoto($this->data['id'])){
	    		$this->code=1;
	    		$this->msg=$res['status'];
	    		$this->details=$photos;
	    	} else $this->msg=self::t("No photo to show");    		
    		
    	} else $this->msg=self::t("Task not found");
    	    	
    	$this->output();
    }

	// public function actionUploadCategoriePhoto()
    // {
    	
    // 	$this->data=$_REQUEST;
    // 	$request=json_encode($_REQUEST);
    	
    // 	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    // 		$this->msg=self::t("Token not valid");
    // 		echo "$this->code|$this->msg|$this->details|".$request;
    // 		Yii::app()->end();
    // 	} 
    	
    // 	$driver_id=$token['driver_id'];  
    // 	$driver_name=$token['first_name'] ." " .$token['last_name'];  
    	
    // 	Driver::setCustomerTimezone( $token['customer_id'] );  	
    // 	if ( $res=Driver::getCategorieByID($this->data['id'], $token['customer_id']) ){    		
    	
    // 		 $contact_id=$res['id'];
    		
    // 		 $path_to_upload=Driver::driverUploadGeoPath();
	// 		 if(isset($_FILES['file'])){
    		 	
    // 		 	header('Access-Control-Allow-Origin: *');
    		 	
    // 		 	$new_image_name = urldecode($_FILES["file"]["name"]).".jpg";	
	// 	        $new_image_name=str_replace(array('?',':'),'',$new_image_name);
		        
	// 	        if(@move_uploaded_file($_FILES["file"]["tmp_name"], "$path_to_upload/".$new_image_name)){		        
	// 		        $params=array(
	// 				   'driver_id'=>$driver_id,
	// 		           'date_created'=>AdminFunctions::dateNow(),
	// 		           'ip_address'=>$_SERVER['REMOTE_ADDR'],	
	// 		           'photo_name'=>$new_image_name
	// 		        );
			        
	// 		        $db=new DbExt;
	// 		        if($db->updateData("{{product_cats}}",$params,'categorie_id',$this->data['id'])){
	// 		           $this->code=1;
	// 			       $this->msg=self::t("Upload successful");
	//     		       $this->details=$contact_id;
	    		       
	//     		       $photo_link=websiteUrl()."/upload/product_cats/".$new_image_name;
	    		       
	//     		    //    $task_info=$res;
    // 	 			//    $task_info['photo_link']=$photo_link;
    	 				    	 			
    // 	 			//    if ( $task_info['trans_type']=="delivery"){ 
    // 	 			// 	   Driver::sendCustomerNotification('DELIVERY_PHOTO',$task_info);
    // 	 			//    } else Driver::sendCustomerNotification('PICKUP_PHOTO',$task_info);	    		       
	    		       
	// 		        } else $this->msg=self::t("failed cannot insert record");
	// 	        } else $this->msg=self::t("Cannot upload photo");
		        		            		    
    // 		 } else $this->msg=self::t("Image is missing");
    		 
    // 	} else $this->msg=self::t("categorie not found");	
    	
    // 	echo "$this->code|$this->msg|$this->details|".$request;
    // }
    
    public function actionGetCategoriePhoto()
    {
    
		if ( $res=Driver::getCategorieByID($this->data['id']) ){    		    		
    		if ( $photos=Driver::getTaskPhoto($this->data['id'])){
	    		$this->code=1;
	    		$this->msg=$res['status'];
	    		$this->details=$photos;
	    	} else $this->msg=self::t("No photo to show");    		
    		
    	} else $this->msg=self::t("Task not found");
    	    	
    	$this->output();
    }
    
    public function actionreRegisterDevice()
    {
    	$new_device_id = isset($this->data['new_device_id'])?$this->data['new_device_id']:'';
		if(empty($new_device_id)){
			$this->msg = $this->t("New device id is empty");
			$this->output();
		}
		
		if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	
    	$driver_id=$token['driver_id']; 
    	
    	$db=new DbExt();
    	
    	$params = array(
    	  'device_id'=>$new_device_id,
    	  'device_platform'=>isset($this->data['device_platform'])?$this->data['device_platform']:'',
    	  'app_version'=>isset($this->data['app_version'])?$this->data['app_version']:'',
    	);
		if ($db->updateData("{{driver}}",$params,'driver_id',$driver_id)){
			$this->code = 1;
			$this->msg = "OK";
			$this->details = $new_device_id;
		} else $this->msg = "Failed cannot update";
		$this->output();
    } 

public function actiongetNewTask()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	
    	if(Driver::getNewTask( $token['driver_id'] )){
    		$this->code = 1;
    		$this->msg = Driver::t("You have new task.");
    	} else $this->msg = Driver::t("No new task");
    	$this->output();
    }
    
	/* new app admin mobilog */
	
	public function actionAddTask()
	{
		
		/*dump($this->data);
		die();*/
		
		$DbExt=new DbExt; 		
		$req=array(
		  'trans_type'=>Driver::t("Transaction type is required"),
		  'customer_name'=>Driver::t("Customer name is required")
		);
		
		if(!isset($this->data['driver_id'])){
			$this->data['driver_id']='';
		}
				
		$Validator=new Validator;
		$Validator->required($req,$this->data);
		if($Validator->validate()){
			
			$params=array(
			  'task_description'=>isset($this->data['task_description'])?$this->data['task_description']:'',
			  'trans_type'=>isset($this->data['trans_type'])?$this->data['trans_type']:'',
			  'contact_number'=>isset($this->data['contact_number'])?$this->data['contact_number']:'',
			  'email_address'=>isset($this->data['email_address'])?$this->data['email_address']:'',
			  'customer_name'=>isset($this->data['customer_name'])?$this->data['customer_name']:'',
			  'delivery_date'=>isset($this->data['delivery_date'])?$this->data['delivery_date']:'',
			  'delivery_address'=>isset($this->data['delivery_address'])?$this->data['delivery_address']:'',
// 			  'finder_id'=>isset($this->data['finder_id'])?$this->data['finder_id']:0,
			  'team_id'=>isset($this->data['team_id'])?$this->data['team_id']:'',
			  'driver_id'=>is_numeric($this->data['driver_id'])?$this->data['driver_id']:0,
			  'task_lat'=>isset($this->data['task_lat'])?$this->data['task_lat']:'',
			  'task_lng'=>isset($this->data['task_lng'])?$this->data['task_lng']:'',
			  'date_created'=>AdminFunctions::dateNow(),
			  'ip_address'=>$_SERVER['REMOTE_ADDR'],			  
			  'customer_id'=>Driver::getUserId(),
			  'dropoff_contact_name'=>isset($this->data['dropoff_contact_name'])?$this->data['dropoff_contact_name']:'',
			  'dropoff_contact_number'=>isset($this->data['dropoff_contact_number'])?$this->data['dropoff_contact_number']:'',
			  'drop_address'=>isset($this->data['drop_address'])?$this->data['drop_address']:'',
			  'dropoff_task_lat'=>isset($this->data['dropoff_task_lat'])?$this->data['dropoff_task_lat']:'',
			  'dropoff_task_lng'=>isset($this->data['dropoff_task_lng'])?$this->data['dropoff_task_lng']:'',
			  'task_token'=>Driver::generateTaskToken()
			);		

			if(empty($params['drop_address'])){
				$params['dropoff_task_lat']='';
				$params['dropoff_task_lng']='';
			}			
						
			if(!empty($params['delivery_date'])){
				$params['delivery_date']= date("Y-m-d G:i",strtotime($params['delivery_date']));
			}
			if($params['driver_id']>0){
				$params['status']='assigned';
			}
						
			if(is_numeric($this->data['task_id'])){
				
				unset($params['date_created']);				
				unset($params['user_id']);
				unset($params['task_token']);
				$params['date_modified']=AdminFunctions::dateNow();				
				
				$task_info=Driver::getTaskId($this->data['task_id']);
				if( $task_info['status']!="unassigned"){
					unset($params['status']);
				}
								
				if ( $DbExt->updateData("{{driver_task}}",$params,'task_id',$this->data['task_id'])){
					$this->code=1;
					$this->msg=Driver::t("Successfully updated");
										
					if (isset($params['status'])){
						if ($params['status']=="assigned"){
							/*add to history*/
							$assigned_task=$params['status'];
							//if ( $res=Driver::getTaskId($this->data['task_id'])){
							if($task_info){
								$status_pretty = Driver::prettyStatus($task_info['status'],$assigned_task);
								$params_history=array(								  
								  'remarks'=>$status_pretty,
								  'status'=>$assigned_task,
								  'date_created'=>AdminFunctions::dateNow(),
								  'ip_address'=>$_SERVER['REMOTE_ADDR'],
								  'task_id'=>$this->data['task_id']
								);		
								$DbExt->insertData('{{task_history}}',$params_history);	
								
								// send notification to driver							
							    Driver::sendDriverNotification('ASSIGN_TASK',$task_info);
							    
							}				
						} 
					} else {						
				        Driver::sendDriverNotification('UPDATE_TASK',$task_info);
					}
					
				} else $this->msg=Driver::t("failed cannot update record");
			} else {
				
				/*plan check*/
				if(!Driver::planCheckCAnAddTask( Driver::getUserId(),Driver::getPlanID() )){
					$this->msg=t("You cannot add more task you account is restrict to add new task");
					$this->jsonResponse();
					Yii::app()->end();
				}
		
				if($DbExt->insertData("{{driver_task}}",$params)){
					$task_id=Yii::app()->db->getLastInsertID();
					$this->code=1;
					$this->msg=Driver::t("Successful");
					
					// send notification to driver
					if ( $info=Driver::getTaskId($task_id)){				
				       Driver::sendDriverNotification('ASSIGN_TASK',$info);
			        }		
			        
			        //send notification to customer	
			        $params['task_id']=$task_id;
			        if ($params['trans_type']=="delivery"){			           
				       Driver::sendCustomerNotification("DELIVERY_TASK_CREATED",$params);
			       } else Driver::sendCustomerNotification("PICKUP_TASK_CREATED",$params);			
					
				} else $this->msg=Driver::t("failed cannot insert record");
			}
		} else $this->msg=$Validator->getErrorAsHTML();
		$this->jsonResponse();
	}
	
	
    
} /*end class*/