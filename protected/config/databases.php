<?php

//==================================================================================================
//  DATABASES
//==================================================================================================

$_databases = array(
    'karterodb@localhost:3306' => array(
        'label'    => 'Mobilog Software',                       // A label that you can quickly identify the database.
        'name'     => 'karterodb',                       // Database name.
        'server'   => 'localhost',                      // Database server.
        'username' => 'root',                           // Database username.
        'password' => '',                               // Database password.
        'port'     => '3306',                           // Database port.
        'encoding' => 'utf8',                           // Database encoding.
        'active'   => true,                             // If the database is visible in the application.
        'tables'   => array(
            'kt_driver' => array(
                'label'      => 'driver',              // A label that you can quickly identify the table.
                'active'     => true,                   // If the table is visible in the application.
                'properties' => array(
                    array(
                        'name'    => 'kt_driver',         // Table name.
                        'columns' =>  array(                          // Columns used - '*' to use all or array with columns options.  
                            array(
                                'label'   => 'ID',          // A label that you can quickly identify the column.
                                'name'    => 'driver_id',  // Columns name.
                                'active'  => true           // If the column is visible in the application.
                            ), 
                          array(
                                'label'   => 'First Name',          // A label that you can quickly identify the column.
                                'name'    => 'first_name',  // Columns name.
                                'active'  => true           // If the column is visible in the application.
                            ), 
                          array(
                                'label'   => 'Last Name',          // A label that you can quickly identify the column.
                                'name'    => 'last_name',  // Columns name.
                                'active'  => true           // If the column is visible in the application.
                            ), 
                          array(
                                'label'   => 'Email',          // A label that you can quickly identify the column.
                                'name'    => 'email',  // Columns name.
                                'active'  => true           // If the column is visible in the application.
                            ), 
                          array(
                                'label'   => 'Phone',          // A label that you can quickly identify the column.
                                'name'    => 'phone',  // Columns name.
                                'active'  => true           // If the column is visible in the application.
                            ), 
                          )
                    )
                )
            ),
//             'products' => array(
//                 'label'      => 'Products',
//                 'active'     => true,
//                 'properties' => array(
//                     array(
//                         'name'      => 'products',
//                         'columns'   => array(
//                             array(
//                                 'label'   => 'ID',          // A label that you can quickly identify the column.
//                                 'name'    => 'id_product',  // Columns name.
//                                 'active'  => true           // If the column is visible in the application.
//                             ),
//                             array(
//                                 'label'   => 'Name',
//                                 'name'    => 'name2',
//                                 'active'  => true
//                             )
//                         )
//                     )
//                 )
//             ),
//             'coupons' => array(
//                 'label'      => 'coupons',
//                 'active'     => true,
//                 'properties' => array(
//                     array(
//                         'name'    => 'coupons',
//                         'columns'      => '*',
//                         'hide_columns' => array('id_coupon')    // To hide specific columns array('id_coupon','status'), not a string.
//                     )
//                 )
//             )
        )
    ),
//     'other' => array(
//         'label'    => 'OTHER',                          // A label so you can quickly identify the database.
//         'name'     => 'car-shop-2',                     // Database name.
//         'server'   => '49.123.45.69',                   // Database server.
//         'username' => 'db_user',                        // Database username.
//         'password' => 'js#djh3$s',                      // Database password.
//         // port     - Since the default port value is 3306, you can comment this line and the database port will be 3306 automatically.
//         // encoding - Since the default encoding is utf8, you can comment this line and the database encoding will be utf8.
//         // active   - Since the default active value is true, you can comment this line and it will also show the database.
//         // tables   - Since the default table value is '*', you can comment this line and it will show all the tables of the database.
//     ),
//     'shortest_possible' => array(
//         'name'     => 'car-shop-3',                     // Database name.
//         'server'   => '49.123.45.69',                   // Database server.
//         'username' => 'db_user',                        // Database username.
//         'password' => 'js#djh3$s',                      // Database password.
//     )
);
?>

