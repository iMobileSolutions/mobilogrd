#!/usr/bin/env bash

if [[ "$1" == "update_beta" ]]; then    # used only on server

    cd /jet/www/default/beta/
    git reset
    git checkout .
    sudo git pull origin develop
#    echo "Nao implementado"

elif [[ "$1" == "update_prod" ]]; then    # used only on server

    cd /home/infokartero/mobilogrd
    sudo git reset
    sudo git checkout .
    sudo git pull origin master

elif [[ "$2" == "" ]]; then

    echo "Falta o segundo parametro (usuário ssh)"

else
    if [[ "$1" == "beta" ]]; then

        git push origin develop
        #ssh $2@35.247.250.238 "source /jet/www/default/beta/update.sh update_beta"

    elif [[ "$1" == "prod" ]]; then

        git push origin master
        ssh $2@35.238.45.43 "source /home/infokartero/mobilogrd/update.sh update_prod"

    elif [[ "$1" == "force-prod" ]]; then
        git push
        git checkout master
        git merge develop
        git pull origin master --ff-only
        git push origin master
        ssh $2@35.238.45.43 "source /home/infokartero/mobilogrd/update.sh update_prod"
        git checkout develop

        echo "Merge feito da develop para a master antes de subir"
    else
        echo "Erro: passe 'beta' ou 'prod' como parâmetro"
    fi
fi

